/*
  Copyright (c) 2016, Matthias Schiffer <mschiffer@universe-factory.net>
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice,
       this list of conditions and the following disclaimer.
    2. Redistributions in binary form must reproduce the above copyright notice,
       this list of conditions and the following disclaimer in the documentation
       and/or other materials provided with the distribution.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

/**
  \file

  ECE access library - implementation
*/


#include "libece.h"

#include <libubox/blobmsg_json.h>


/** Timeout for libece ubus calls (in ms) */
#define LIBECE_TIMEOUT 1000


/** blob_buf reused for all ubus calls */
static struct blob_buf b = {};


/** Cleans up after libece */
__attribute__((destructor)) static void libece_done(void) {
	blob_buf_free(&b);
}


/** ubus reply handler for the "get" operation */
static void libece_handle_get(struct ubus_request *req, int type, struct blob_attr *msg) {
	if (!msg || type != UBUS_MSG_DATA)
		return;

	struct blob_attr **ret = req->priv;
	*ret = blob_memdup(msg);
}

struct blob_attr * libece_get(struct ubus_context *ctx, const char *path, int *error) {
	int err;

	uint32_t id;
	err = ubus_lookup_id(ctx, "ece", &id);
	if (err) {
		if (error)
			*error = err;
		return NULL;
	}

	blob_buf_init(&b, 0);
	if (blobmsg_add_string(&b, "path", path)) {
		if (error)
			*error = UBUS_STATUS_UNKNOWN_ERROR;
		return NULL;
	}

	struct blob_attr *ret = NULL;
	err = ubus_invoke(ctx, id, "get", b.head, libece_handle_get, &ret, LIBECE_TIMEOUT);
	if (err) {
		if (error)
			*error = err;
		return NULL;
	}

	if (error)
		*error = UBUS_STATUS_OK;
	return ret;
}

/** Helper used by all set/insert/extend/delete functions */
static int _libece_set(struct ubus_context *ctx, const char *path) {
	int err;

	uint32_t id;
	err = ubus_lookup_id(ctx, "ece", &id);
	if (err)
		return err;

	if (blobmsg_add_string(&b, "path", path))
		return UBUS_STATUS_UNKNOWN_ERROR;

	err = ubus_invoke(ctx, id, "set", b.head, NULL, NULL, LIBECE_TIMEOUT);
	if (err)
		return err;

	return UBUS_STATUS_OK;
}

int libece_set(struct ubus_context *ctx, const char *path, const struct blob_attr *value) {
	blob_buf_init(&b, 0);

	if (blobmsg_add_field(&b, blobmsg_type(value), "value", blobmsg_data(value), blobmsg_data_len(value)))
		return UBUS_STATUS_UNKNOWN_ERROR;

	return _libece_set(ctx, path);
}

int libece_insert(struct ubus_context *ctx, const char *path, const struct blob_attr *value) {
	blob_buf_init(&b, 0);

	void *c = blobmsg_open_array(&b, "insert");
	if (!c)
		return UBUS_STATUS_UNKNOWN_ERROR;

	if (blobmsg_add_field(&b, blobmsg_type(value), NULL, blobmsg_data(value), blobmsg_data_len(value)))
		return UBUS_STATUS_UNKNOWN_ERROR;

	blobmsg_close_array(&b, c);

	return _libece_set(ctx, path);
}

int libece_insert_list(struct ubus_context *ctx, const char *path, const struct blob_attr *value_list) {
	blob_buf_init(&b, 0);

	if (blobmsg_add_field(&b, blobmsg_type(value_list), "insert", blobmsg_data(value_list), blobmsg_data_len(value_list)))
		return UBUS_STATUS_UNKNOWN_ERROR;

	return _libece_set(ctx, path);
}

int libece_extend(struct ubus_context *ctx, const char *path, const struct blob_attr *prepend, const struct blob_attr *append) {
	if (!(prepend || append))
		return UBUS_STATUS_INVALID_ARGUMENT;

	blob_buf_init(&b, 0);

	if (prepend) {
		void *c = blobmsg_open_array(&b, "prepend");
		if (!c)
			return UBUS_STATUS_UNKNOWN_ERROR;

		if (blobmsg_add_field(&b, blobmsg_type(prepend), NULL, blobmsg_data(prepend), blobmsg_data_len(prepend)))
			return UBUS_STATUS_UNKNOWN_ERROR;

		blobmsg_close_array(&b, c);
	}

	if (append) {
		void *c = blobmsg_open_array(&b, "append");
		if (!c)
			return UBUS_STATUS_UNKNOWN_ERROR;

		if (blobmsg_add_field(&b, blobmsg_type(append), NULL, blobmsg_data(append), blobmsg_data_len(append)))
			return UBUS_STATUS_UNKNOWN_ERROR;

		blobmsg_close_array(&b, c);
	}

	return _libece_set(ctx, path);
}

int libece_extend_list(struct ubus_context *ctx, const char *path, const struct blob_attr *prepend_list, const struct blob_attr *append_list) {
	if (!(prepend_list || append_list))
		return UBUS_STATUS_INVALID_ARGUMENT;

	blob_buf_init(&b, 0);

	if (prepend_list) {
		if (blobmsg_add_field(&b, blobmsg_type(prepend_list), "prepend", blobmsg_data(prepend_list), blobmsg_data_len(prepend_list)))
			return UBUS_STATUS_UNKNOWN_ERROR;
	}

	if (append_list) {
		if (blobmsg_add_field(&b, blobmsg_type(append_list), "append", blobmsg_data(append_list), blobmsg_data_len(append_list)))
			return UBUS_STATUS_UNKNOWN_ERROR;
	}

	return _libece_set(ctx, path);
}

int libece_delete(struct ubus_context *ctx, const char *path) {
	blob_buf_init(&b, 0);

	return _libece_set(ctx, path);
}

int libece_reset(struct ubus_context *ctx, const char *path) {
	int err;

	uint32_t id;
	err = ubus_lookup_id(ctx, "ece", &id);
	if (err)
		return err;

	blob_buf_init(&b, 0);

	if (blobmsg_add_string(&b, "path", path))
		return UBUS_STATUS_UNKNOWN_ERROR;

	err = ubus_invoke(ctx, id, "reset", b.head, NULL, NULL, LIBECE_TIMEOUT);
	if (err)
		return err;

	return UBUS_STATUS_OK;
}
