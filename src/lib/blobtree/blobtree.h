/*
  Copyright (c) 2016, Matthias Schiffer <mschiffer@universe-factory.net>
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice,
       this list of conditions and the following disclaimer.
    2. Redistributions in binary form must reproduce the above copyright notice,
       this list of conditions and the following disclaimer in the documentation
       and/or other materials provided with the distribution.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

/**
  \file

  Tree-based blobmsg representation for efficient modification
*/


#pragma once


#include <libubox/avl.h>
#include <libubox/blobmsg.h>
#include <libubox/list.h>

#include <assert.h>
#include <string.h>


/** Alignment of blobtree allocations */
#define ECE_BLOBTREE_ALIGN 8

/** blobtree alignment attribute */
#define ECE_BLOBTREE_ALIGNED __attribute__((aligned(ECE_BLOBTREE_ALIGN)))


/** A blobmsg tree */
typedef struct _ece_blobtree {
	unsigned _type : 8;       /**< The type of the blob represented by this blobtree */
	unsigned _length : 24;    /**< The length of the payload */
	uint16_t _hdrlen;         /**< The offset of the payload (relative to the data field) */
	char _data[] ECE_BLOBTREE_ALIGNED; /**< Header and payload data */
} ece_blobtree_t;

/** Payload of array nodes */
typedef struct ECE_BLOBTREE_ALIGNED _ece_blobtree_array {
	size_t _length;           /**< The number of items in the array */
	struct list_head _list;   /**< Item list */
} _ece_blobtree_array_t;

/** Header of array items */
typedef struct ECE_BLOBTREE_ALIGNED _ece_blobtree_array_header {
	struct list_head _list;   /**< Array entry */
} _ece_blobtree_array_header_t;

/** Payload of table nodes */
typedef struct ECE_BLOBTREE_ALIGNED _ece_blobtree_table {
	struct avl_tree _entries; /**< Table entries */
} _ece_blobtree_table_t;

/** Header of table entries */
typedef struct ECE_BLOBTREE_ALIGNED _ece_blobtree_table_header {
	struct avl_node _node;    /**< Table entry */
	char _name[];             /**< Key of the table entry */
} _ece_blobtree_table_header_t;


/** Returns the (blobmsg) type of a tree object */
static inline int ece_blobtree_type(const ece_blobtree_t *tree) {
	return tree->_type;
}

/** Returns a const pointer to the underlying blobmsg's payload */
static inline const void * _ece_blobtree_data_const(const ece_blobtree_t *tree) {
	return tree->_data + tree->_hdrlen;
}

/** Returns a pointer to the header of the blobtree element */
static inline void * _ece_blobtree_header(ece_blobtree_t *tree) {
	return tree->_data;
}

/** Calculates the pointer to an ece_blobtree_t from a pointer to its header */
static inline ece_blobtree_t * _ece_blobtree_unheader(void *header) {
	return container_of(header, ece_blobtree_t, _data);
}

/** Returns the array payload of a blobtree */
static inline const _ece_blobtree_array_t * _ece_blobtree_get_array(const ece_blobtree_t *tree) {
	assert(ece_blobtree_type(tree) == BLOBMSG_TYPE_ARRAY);
	return _ece_blobtree_data_const(tree);
}

/** Returns the table payload of a blobtree */
static inline const _ece_blobtree_table_t * _ece_blobtree_get_table(const ece_blobtree_t *tree) {
	assert(ece_blobtree_type(tree) == BLOBMSG_TYPE_TABLE);
	return _ece_blobtree_data_const(tree);
}


/** Returns the value of a int8 blobtree */
static inline uint8_t ece_blobtree_get_u8(const ece_blobtree_t *tree) {
	assert(ece_blobtree_type(tree) == BLOBMSG_TYPE_INT8);
	return *(uint8_t *)_ece_blobtree_data_const(tree);
}

/** Returns the value of a int16 blobtree */
static inline uint16_t ece_blobtree_get_u16(const ece_blobtree_t *tree) {
	assert(ece_blobtree_type(tree) == BLOBMSG_TYPE_INT16);
	return be16_to_cpu(*(uint16_t *)_ece_blobtree_data_const(tree));
}

/** Returns the value of a int32 blobtree */
static inline uint32_t ece_blobtree_get_u32(const ece_blobtree_t *tree) {
	assert(ece_blobtree_type(tree) == BLOBMSG_TYPE_INT32);
	return be32_to_cpu(*(uint32_t *)_ece_blobtree_data_const(tree));
}

/** Returns the value of a int64 blobtree */
static inline uint64_t ece_blobtree_get_u64(const ece_blobtree_t *tree) {
	assert(ece_blobtree_type(tree) == BLOBMSG_TYPE_INT64);
	return be64_to_cpu(*(uint64_t *)_ece_blobtree_data_const(tree));
}

/** Returns the value of a bool blobtree */
static inline bool ece_blobtree_get_bool(const ece_blobtree_t *tree) {
	return ece_blobtree_get_u8(tree);
}

/** Returns the value of a string blobtree */
static inline const char * ece_blobtree_get_string(const ece_blobtree_t *tree) {
	assert(ece_blobtree_type(tree) == BLOBMSG_TYPE_STRING);
	return _ece_blobtree_data_const(tree);
}


/** Creates a new primitive blobtree with given type and payload */
ece_blobtree_t * _ece_blobtree_new(int type, const void *data, size_t length);

/** Create a new empty array blobtree */
ece_blobtree_t * ece_blobtree_new_array(void);

/** Creates a new empty table blobtree */
ece_blobtree_t * ece_blobtree_new_table(void);

/** Creates a new empty (null) blobtree */
static inline ece_blobtree_t * ece_blobtree_new_empty(void) {
	return _ece_blobtree_new(BLOBMSG_TYPE_UNSPEC, NULL, 0);
}

/** Creates a new int8 blobtree with a given value */
static inline ece_blobtree_t * ece_blobtree_new_u8(uint8_t value) {
	return _ece_blobtree_new(BLOBMSG_TYPE_INT8, &value, sizeof(value));
}

/** Creates a new int16 blobtree with a given value */
static inline ece_blobtree_t * ece_blobtree_new_u16(uint16_t value) {
	uint16_t be = cpu_to_be16(value);
	return _ece_blobtree_new(BLOBMSG_TYPE_INT16, &be, sizeof(be));
}

/** Creates a new int32 blobtree with a given value */
static inline ece_blobtree_t * ece_blobtree_new_u32(uint32_t value) {
	uint32_t be = cpu_to_be32(value);
	return _ece_blobtree_new(BLOBMSG_TYPE_INT32, &be, sizeof(be));
}

/** Creates a new int64 blobtree with a given value */
static inline ece_blobtree_t * ece_blobtree_new_u64(uint64_t value) {
	uint64_t be = cpu_to_be64(value);
	return _ece_blobtree_new(BLOBMSG_TYPE_INT64, &be, sizeof(be));
}

/** Creates a new bool blobtree with a given value */
static inline ece_blobtree_t * ece_blobtree_new_bool(bool value) {
	return ece_blobtree_new_u8(value);
}

/** Creates a new string blobtree with a given value */
static inline ece_blobtree_t * ece_blobtree_new_string(const char *value) {
	return _ece_blobtree_new(BLOBMSG_TYPE_STRING, value, strlen(value)+1);
}


/** Returns the length of an array blobtree */
static inline size_t ece_blobtree_array_length(const ece_blobtree_t *tree) {
	return _ece_blobtree_get_array(tree)->_length;
}


/**
  Iterates over the items of an array blobtree

  item must be a ece_blobtree_t *.
*/
#define ece_blobtree_array_for_each(tree, item) \
	for ( \
		item = _ece_blobtree_unheader(list_first_entry(&_ece_blobtree_get_array(tree)->_list, _ece_blobtree_array_header_t, _list)); \
		&((_ece_blobtree_array_header_t *)_ece_blobtree_header(item))->_list != &_ece_blobtree_get_array(tree)->_list; \
		item = _ece_blobtree_unheader(list_entry(((_ece_blobtree_array_header_t *)_ece_blobtree_header(item))->_list.next, _ece_blobtree_array_header_t, _list)) \
	)

/**
  Iterates over the items of an array blobtree (safe against deletions)

  item and next must be ece_blobtree_t *.
*/
#define ece_blobtree_array_for_each_safe(tree, item, next) \
	for ( \
		item = _ece_blobtree_unheader(list_first_entry(&_ece_blobtree_get_array(tree)->_list, _ece_blobtree_array_header_t, _list)), \
		next = _ece_blobtree_unheader(list_entry(((_ece_blobtree_array_header_t *)_ece_blobtree_header(item))->_list.next, _ece_blobtree_array_header_t, _list)); \
		&((_ece_blobtree_array_header_t *)_ece_blobtree_header(item))->_list != &_ece_blobtree_get_array(tree)->_list; \
		item = next, \
		next = _ece_blobtree_unheader(list_entry(((_ece_blobtree_array_header_t *)_ece_blobtree_header(item))->_list.next, _ece_blobtree_array_header_t, _list)) \
	)

/**
  Iterates over the elements of an table blobtree

  key must be a const char *, item a ece_blobtree_t *.
*/
#define ece_blobtree_table_for_each(tree, key, value) \
	for ( \
		value = _ece_blobtree_unheader(list_first_entry(&_ece_blobtree_get_table(tree)->_entries.list_head, _ece_blobtree_table_header_t, _node.list)), \
		key = (const char*)((_ece_blobtree_table_header_t *)_ece_blobtree_header(value))->_name; \
		&((_ece_blobtree_table_header_t *)_ece_blobtree_header(value))->_node.list != &_ece_blobtree_get_table(tree)->_entries.list_head; \
		value = _ece_blobtree_unheader(list_entry(((_ece_blobtree_table_header_t *)_ece_blobtree_header(value))->_node.list.next, _ece_blobtree_table_header_t, _node.list)), \
		key = (const char*)((_ece_blobtree_table_header_t *)_ece_blobtree_header(value))->_name \
	)

/**
  Iterates over the elements of an table blobtree (safe against deletions)

  key must be a const char *, item and next ece_blobtree_t *.
*/
#define ece_blobtree_table_for_each_safe(tree, key, value, next) \
	for ( \
		value = _ece_blobtree_unheader(list_first_entry(&_ece_blobtree_get_table(tree)->_entries.list_head, _ece_blobtree_table_header_t, _node.list)), \
		key = (const char*)((_ece_blobtree_table_header_t *)_ece_blobtree_header(value))->_name, \
		next = _ece_blobtree_unheader(list_entry(((_ece_blobtree_table_header_t *)_ece_blobtree_header(value))->_node.list.next, _ece_blobtree_table_header_t, _node.list)); \
		&((_ece_blobtree_table_header_t *)_ece_blobtree_header(value))->_node.list != &_ece_blobtree_get_table(tree)->_entries.list_head; \
		value = next, \
		key = (const char*)((_ece_blobtree_table_header_t *)_ece_blobtree_header(value))->_name, \
		next = _ece_blobtree_unheader(list_entry(((_ece_blobtree_table_header_t *)_ece_blobtree_header(value))->_node.list.next, _ece_blobtree_table_header_t, _node.list)) \
	)


/**
  Returns the length of the given array

  tree must be an array.
*/
size_t ece_blobtree_array_length(const ece_blobtree_t *tree);

/**
  Returns the array item with the given index (or NULL)

  tree must be an array.
*/
ece_blobtree_t * ece_blobtree_array_get_item(const ece_blobtree_t *tree, size_t index);

/**
  Inserts a new item into an array at a given index.

  tree must be an array.

  Will return false and set errno in the following cases:

  ENOMEM: memory allocation failure
  ENOENT: the given index does not exist
*/
bool ece_blobtree_array_insert_item(ece_blobtree_t *tree, size_t index, ece_blobtree_t *value);

/**
  Inserts an array of items into an array at a given index.

  tree must be an array.

  Will return false and set errno in the following cases:

  ENOENT: the given index does not exist
  EINVAL: value isn't an array
*/
bool ece_blobtree_array_insert_items(ece_blobtree_t *tree, size_t index, ece_blobtree_t *values);

/**
  Appends a new item at the end of an array.

  tree must be an array.

  Will return false and set errno in the following cases:

  ENOMEM: memory allocation failure
*/
bool ece_blobtree_array_append_item(ece_blobtree_t *tree, ece_blobtree_t *value);

/**
  Append an array of items at the end of an array.

  tree must be an array.

  Will return false and set errno in the following cases:

  EINVAL: value isn't an array
*/
bool ece_blobtree_array_append_items(ece_blobtree_t *tree, ece_blobtree_t *values);

/**
  Replaces the element at the given array index with the given blobmsg

  tree must be an array.

  Will return false and set errno in the following cases:

  ENOMEM: memory allocation failure
  ENOENT: the given index does not exist
*/
bool ece_blobtree_array_replace_item(ece_blobtree_t *tree, size_t index, ece_blobtree_t *value);

/**
  Removes and returns the element at the given array index

  Returns false if the index doesn't exist.
*/
ece_blobtree_t * ece_blobtree_array_remove_item(ece_blobtree_t *tree, size_t index);

/**
  Removes and frees the element at the given array index

  Returns false if the index doesn't exist.
*/
bool ece_blobtree_array_delete_item(ece_blobtree_t *tree, size_t index);

/**
  Returns the table property with the given name (or NULL)

  tree must be a table.
*/
ece_blobtree_t * ece_blobtree_table_get_property(const ece_blobtree_t *tree, const char *name);

/**
  Inserts or replaces the table property with the given name

  tree must be a table.

  Will return false and set errno in the following cases:

  ENOMEM: memory allocation failure
*/
bool ece_blobtree_table_insert_property(ece_blobtree_t *tree, const char *name, ece_blobtree_t *value);

/**
  Removes and returns the table property with the given name

  tree must be a table.
*/
ece_blobtree_t * ece_blobtree_table_remove_property(ece_blobtree_t *tree, const char *name);

/**
  Removes and frees the table property with the given name

  tree must be a table.
*/
void ece_blobtree_table_delete_property(ece_blobtree_t *tree, const char *name);

/**
  Writes the blobtree to the given buffer in blobmsg format

  Name may be NULL. May fail because of memory allocation failures.
*/
bool ece_blobtree_put(struct blob_buf *b, const ece_blobtree_t *tree, const char *name);

/**
  Converts a blobmsg into a blobtree

  Will return NULL and set errno in the following cases:

  ENOMEM: memory allocation failure
  EINVAL: blob is an invalid blobmsg
*/
ece_blobtree_t * ece_blobtree_parse(const struct blob_attr *blob);

/**
  Creates an identical (deep) copy of a blobtree

  Will return NULL and set errno to ENOMEM on allocation failures.
*/
ece_blobtree_t * ece_blobtree_clone(const ece_blobtree_t *tree);

/** Frees the memory used by a blobtree */
void ece_blobtree_free(ece_blobtree_t *tree);
