/*
  Copyright (c) 2016, Matthias Schiffer <mschiffer@universe-factory.net>
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice,
       this list of conditions and the following disclaimer.
    2. Redistributions in binary form must reproduce the above copyright notice,
       this list of conditions and the following disclaimer in the documentation
       and/or other materials provided with the distribution.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

/**
  \file

  Tree-based blobmsg representation for efficient modification - implementation
*/


#include "blobtree.h"

#include <libubox/avl-cmp.h>

#include <errno.h>


/** Assert that always executes possible side effects of the condition */
static inline void _ece_blobtree_assert(bool __attribute__((unused)) cond) {
	assert(cond);
}

/** Returns a pointer to the underlying blobmsg's payload */
static inline void * _ece_blobtree_data(ece_blobtree_t *tree) {
	return tree->_data + tree->_hdrlen;
}

/** Calculates the header length for a table entry with a given name length */
static inline size_t _ece_blobtree_table_header_len(size_t namelen) {
	return (sizeof(_ece_blobtree_table_header_t) + namelen+1 + ECE_BLOBTREE_ALIGN-1) & ~(ECE_BLOBTREE_ALIGN-1);
}

/**
  Allocates a new blobtree node

  If the allocation fails, NULL is returned and errno is set to ENOMEM.
*/
static ece_blobtree_t * _ece_blobtree_alloc(int type, size_t hdrlen, size_t payload) {
	if (hdrlen > UINT16_MAX) {
		errno = EINVAL;
		return NULL;
	}

	ece_blobtree_t *ret = malloc(sizeof(*ret) + hdrlen + payload);
	if (!ret) {
		errno = ENOMEM;
		return NULL;
	}

	ret->_type = type;
	ret->_length = payload;
	ret->_hdrlen = hdrlen;

	errno = 0;
	return ret;
}

ece_blobtree_t * _ece_blobtree_new(int type, const void *data, size_t length) {
	assert(type != BLOBMSG_TYPE_ARRAY && type != BLOBMSG_TYPE_TABLE);

	ece_blobtree_t *ret = _ece_blobtree_alloc(type, 0, length);
	if (!ret)
		return NULL;

	if (length)
		memcpy(_ece_blobtree_data(ret), data, length);

	return ret;
}

ece_blobtree_t * ece_blobtree_new_array(void) {
	ece_blobtree_t *ret = _ece_blobtree_alloc(BLOBMSG_TYPE_ARRAY, 0, sizeof(_ece_blobtree_array_t));
	if (!ret)
		return NULL;

	_ece_blobtree_array_t *array = _ece_blobtree_data(ret);
	INIT_LIST_HEAD(&array->_list);
	array->_length = 0;

	return ret;
}

ece_blobtree_t * ece_blobtree_new_table(void) {
	ece_blobtree_t *ret = _ece_blobtree_alloc(BLOBMSG_TYPE_TABLE, 0, sizeof(_ece_blobtree_table_t));
	if (!ret)
		return NULL;

	_ece_blobtree_table_t *table = _ece_blobtree_data(ret);
	avl_init(&table->_entries, avl_strcmp, false, NULL);

	return ret;
}

/**
  Returns a newly allocated simple blobtree node with the given name and the payload of the given blob

  May return NULL on allocations failures.
*/
static ece_blobtree_t * _ece_blobtree_from_blob(size_t hdrlen, const struct blob_attr *blob) {
	unsigned type = blob_id(blob);
	assert(type != BLOBMSG_TYPE_ARRAY && type != BLOBMSG_TYPE_TABLE);

	size_t payload = blobmsg_len(blob);

	ece_blobtree_t *ret = _ece_blobtree_alloc(type, hdrlen, payload);
	if (!ret)
		return NULL;

	memcpy(_ece_blobtree_data(ret), blobmsg_data(blob), payload);

	return ret;
}

/**
  Converts a blobmsg into a blobtree with a given header length

  Will return NULL and set errno in the following cases:

  ENOMEM: memory allocation failure
  EINVAL: blob is an invalid blobmsg
*/
static ece_blobtree_t * _ece_blobtree_parse_with_header(uint16_t hdrlen, const struct blob_attr *blob) {
	int type = blobmsg_type(blob);
	if (type != BLOBMSG_TYPE_ARRAY && type != BLOBMSG_TYPE_TABLE) {
		if (!blobmsg_check_attr(blob, false)) {
			errno = EINVAL;
			return NULL;
		}

		return _ece_blobtree_from_blob(hdrlen, blob);
	}

	if (!blobmsg_check_attr_list(blob, BLOBMSG_TYPE_UNSPEC)) {
		errno = EINVAL;
		return NULL;
	}

	if (type == BLOBMSG_TYPE_ARRAY) {
		ece_blobtree_t *ret = _ece_blobtree_alloc(
			type, hdrlen, sizeof(_ece_blobtree_array_t));
		if (!ret)
			return NULL;

		_ece_blobtree_array_t *array = _ece_blobtree_data(ret);
		INIT_LIST_HEAD(&array->_list);
		array->_length = 0;

		struct blob_attr *cur;
		ssize_t rem;
		blobmsg_for_each_attr(cur, blob, rem) {
			ece_blobtree_t *entry = _ece_blobtree_parse_with_header(
				sizeof(_ece_blobtree_array_header_t), cur);
			if (!entry) {
				int errno_safe = errno;
				ece_blobtree_free(ret);
				errno = errno_safe;
				return NULL;
			}

			_ece_blobtree_array_header_t *header = _ece_blobtree_header(entry);
			list_add_tail(&header->_list, &array->_list);
			array->_length++;
		}

		errno = 0;
		return ret;
	}
	else {
		assert(type == BLOBMSG_TYPE_TABLE);

		ece_blobtree_t *ret = _ece_blobtree_alloc(
			type, hdrlen, sizeof(_ece_blobtree_table_t));
		if (!ret)
			return NULL;

		_ece_blobtree_table_t *table = _ece_blobtree_data(ret);
		avl_init(&table->_entries, avl_strcmp, false, NULL);

		struct blob_attr *cur;
		ssize_t rem;
		blobmsg_for_each_attr(cur, blob, rem) {
			const char *name = blobmsg_name(cur);
			size_t namelen = strlen(name);
			ece_blobtree_t *entry = _ece_blobtree_parse_with_header(_ece_blobtree_table_header_len(namelen), cur);
			if (!entry) {
				int errno_safe = errno;
				ece_blobtree_free(ret);
				errno = errno_safe;
				return NULL;
			}

			_ece_blobtree_table_header_t *header = _ece_blobtree_header(entry);
			memcpy(header->_name, name, namelen+1);
			header->_node.key = header->_name;
			if (avl_insert(&table->_entries, &header->_node) != 0) {
				ece_blobtree_free(entry);
				ece_blobtree_free(ret);
				errno = EINVAL;
				return NULL;
			}
		}

		errno = 0;
		return ret;
	}
}

ece_blobtree_t * ece_blobtree_parse(const struct blob_attr *blob) {
	return _ece_blobtree_parse_with_header(0, blob);
}

/**
  Creates an identical (deep) copy of a blobtree with a given name

  Will return NULL and set errno to ENOMEM on allocation failures.
*/
static ece_blobtree_t * _ece_blobtree_clone_with_header(uint16_t hdrlen, const ece_blobtree_t *tree) {
	int type = tree->_type;
	if (type == BLOBMSG_TYPE_ARRAY) {
		ece_blobtree_t *ret = _ece_blobtree_alloc(
			type, hdrlen, sizeof(_ece_blobtree_array_t));
		if (!ret)
			return NULL;

		_ece_blobtree_array_t *array = _ece_blobtree_data(ret);
		INIT_LIST_HEAD(&array->_list);
		array->_length = 0;

		ece_blobtree_t *item;
		ece_blobtree_array_for_each(tree, item) {
			ece_blobtree_t *entry = _ece_blobtree_clone_with_header(
				sizeof(_ece_blobtree_array_header_t), item);
			if (!entry) {
				int errno_safe = errno;
				ece_blobtree_free(ret);
				errno = errno_safe;
				return NULL;
			}

			_ece_blobtree_array_header_t *header = _ece_blobtree_header(entry);
			list_add_tail(&header->_list, &array->_list);
			array->_length++;
		}

		assert(array->_length == ece_blobtree_array_length(tree));

		errno = 0;
		return ret;
	}
	else if (type == BLOBMSG_TYPE_TABLE) {
		ece_blobtree_t *ret = _ece_blobtree_alloc(
			type, hdrlen, sizeof(_ece_blobtree_table_t));
		if (!ret)
			return NULL;

		_ece_blobtree_table_t *table = _ece_blobtree_data(ret);
		avl_init(&table->_entries, avl_strcmp, false, NULL);

		const char *name;
		ece_blobtree_t *prop;
		ece_blobtree_table_for_each(tree, name, prop) {
			size_t namelen = strlen(name);
			ece_blobtree_t *entry = _ece_blobtree_clone_with_header(_ece_blobtree_table_header_len(namelen), prop);
			if (!entry) {
				int errno_safe = errno;
				ece_blobtree_free(ret);
				errno = errno_safe;
				return NULL;
			}

			_ece_blobtree_table_header_t *header = _ece_blobtree_header(entry);
			memcpy(header->_name, name, namelen+1);
			header->_node.key = header->_name;
			if (avl_insert(&table->_entries, &header->_node) != 0) {
				ece_blobtree_free(entry);
				ece_blobtree_free(ret);
				errno = EINVAL;
				return NULL;
			}
		}

		errno = 0;
		return ret;
	}
	else {
		assert(type != BLOBMSG_TYPE_ARRAY && type != BLOBMSG_TYPE_TABLE);

		ece_blobtree_t *ret = _ece_blobtree_alloc(type, hdrlen, tree->_length);
		if (!ret)
			return NULL;

		memcpy(_ece_blobtree_data(ret), _ece_blobtree_data_const(tree), tree->_length);

		errno = 0;
		return ret;
	}
}

ece_blobtree_t * ece_blobtree_clone(const ece_blobtree_t *tree) {
	return _ece_blobtree_clone_with_header(0, tree);
}

/** Creates a modified version of the given blobtree with a given header length */
static bool _ece_blobtree_adjust_header(uint16_t hdrlen, ece_blobtree_t **treep) {
	ece_blobtree_t *tree = *treep;

	if (tree->_hdrlen == hdrlen)
		return true;

	int type = tree->_type;

	ece_blobtree_t *ret = _ece_blobtree_alloc(type, hdrlen, tree->_length);
	if (!ret)
		return false;


	if (type == BLOBMSG_TYPE_ARRAY) {
		_ece_blobtree_array_t *orig_array = _ece_blobtree_data(tree);
		_ece_blobtree_array_t *array = _ece_blobtree_data(ret);

		INIT_LIST_HEAD(&array->_list);
		list_splice(&orig_array->_list, &array->_list);
		array->_length = orig_array->_length;
	}
	else if (type == BLOBMSG_TYPE_TABLE) {
		_ece_blobtree_table_t *orig_table = _ece_blobtree_data(tree);
		_ece_blobtree_table_t *table = _ece_blobtree_data(ret);
		avl_init(&table->_entries, avl_strcmp, false, NULL);

		_ece_blobtree_table_header_t *prop, *next;
		avl_remove_all_elements(&orig_table->_entries, prop, _node, next)
			_ece_blobtree_assert(avl_insert(&table->_entries, &prop->_node) == 0);
	}
	else {
		assert(type != BLOBMSG_TYPE_ARRAY && type != BLOBMSG_TYPE_TABLE);
		memcpy(_ece_blobtree_data(ret), _ece_blobtree_data_const(tree), tree->_length);
	}

	free(tree);
	*treep = ret;

	errno = 0;
	return true;
}

ece_blobtree_t * ece_blobtree_array_get_item(const ece_blobtree_t *tree, size_t index) {
	if (index >= ece_blobtree_array_length(tree)) {
		errno = ENOENT;
		return NULL;
	}

	ece_blobtree_t *entry;
	ece_blobtree_array_for_each(tree, entry) {
		if (index-- == 0) {
			errno = 0;
			return entry;
		}
	}

	assert(false);
	__builtin_unreachable();
}

/** Returns the list_head before the given index */
static struct list_head * _ece_blobtree_array_get_prev(ece_blobtree_t *tree, size_t index) {
	assert(tree->_type == BLOBMSG_TYPE_ARRAY);

	if (index == 0) {
		_ece_blobtree_array_t *array = _ece_blobtree_data(tree);
		return &array->_list;
	}

	ece_blobtree_t *prev_item = ece_blobtree_array_get_item(tree, index-1);
	if (!prev_item) {
		errno = ENOENT;
		return NULL;
	}

	_ece_blobtree_array_header_t *prev_header = _ece_blobtree_header(prev_item);
	return &prev_header->_list;
}

/** Adds a new item to an array after item prev */
static bool _ece_blobtree_array_add_item(_ece_blobtree_array_t *array, struct list_head *prev, ece_blobtree_t *value) {
	if (!_ece_blobtree_adjust_header(sizeof(_ece_blobtree_array_header_t), &value))
		return false;

	_ece_blobtree_array_header_t *header = _ece_blobtree_header(value);
	list_add(&header->_list, prev);
	array->_length++;

	errno = 0;
	return true;
}

/** Adds a list of new items to an array after item prev */
static bool _ece_blobtree_array_add_items(_ece_blobtree_array_t *array, struct list_head *prev, ece_blobtree_t *values) {
	if (values->_type != BLOBMSG_TYPE_ARRAY) {
		errno = EINVAL;
		return false;
	}

	_ece_blobtree_array_t *item_array = _ece_blobtree_data(values);
	list_splice(&item_array->_list, prev);
	array->_length += item_array->_length;
	free(values);

	errno = 0;
	return true;
}

bool ece_blobtree_array_insert_item(ece_blobtree_t *tree, size_t index, ece_blobtree_t *value) {
	_ece_blobtree_array_t *array = _ece_blobtree_data(tree);

	struct list_head *prev = _ece_blobtree_array_get_prev(tree, index);
	if (!prev)
		return false;

	return _ece_blobtree_array_add_item(array, prev, value);
}

bool ece_blobtree_array_insert_items(ece_blobtree_t *tree, size_t index, ece_blobtree_t *values) {
	_ece_blobtree_array_t *array = _ece_blobtree_data(tree);

	struct list_head *prev = _ece_blobtree_array_get_prev(tree, index);
	if (!prev)
		return false;

	return _ece_blobtree_array_add_items(array, prev, values);
}

bool ece_blobtree_array_append_item(ece_blobtree_t *tree, ece_blobtree_t *value) {
	_ece_blobtree_array_t *array = _ece_blobtree_data(tree);
	struct list_head *prev = array->_list.prev;
	return _ece_blobtree_array_add_item(array, prev, value);
}

bool ece_blobtree_array_append_items(ece_blobtree_t *tree, ece_blobtree_t *values) {
	_ece_blobtree_array_t *array = _ece_blobtree_data(tree);
	struct list_head *prev = array->_list.prev;
	return _ece_blobtree_array_add_items(array, prev, values);
}

bool ece_blobtree_array_replace_item(ece_blobtree_t *tree, size_t index, ece_blobtree_t *value) {
	ece_blobtree_t *old_item = ece_blobtree_array_get_item(tree, index);
	if (!old_item) {
		errno = ENOENT;
		return false;
	}

	_ece_blobtree_array_header_t *old_header = _ece_blobtree_header(old_item);
	struct list_head *prev = old_header->_list.prev;

	if (!_ece_blobtree_adjust_header(sizeof(_ece_blobtree_array_header_t), &value))
		return false;

	list_del(&old_header->_list);
	ece_blobtree_free(old_item);

	_ece_blobtree_array_header_t *header = _ece_blobtree_header(value);
	list_add(&header->_list, prev);

	errno = 0;
	return true;
}

ece_blobtree_t * ece_blobtree_array_remove_item(ece_blobtree_t *tree, size_t index) {
	_ece_blobtree_array_t *array = _ece_blobtree_data(tree);

	ece_blobtree_t *item = ece_blobtree_array_get_item(tree, index);
	if (!item) {
		errno = ENOENT;
		return NULL;
	}

	_ece_blobtree_array_header_t *entry = _ece_blobtree_header(item);
	list_del(&entry->_list);
	array->_length--;

	errno = 0;
	return item;
}

bool ece_blobtree_array_delete_item(ece_blobtree_t *tree, size_t index) {
	ece_blobtree_t *item = ece_blobtree_array_remove_item(tree, index);
	if (!item)
		return false;

	ece_blobtree_free(item);

	return true;
}

ece_blobtree_t * ece_blobtree_table_get_property(const ece_blobtree_t *tree, const char *name) {
	assert(tree->_type == BLOBMSG_TYPE_TABLE);

	const _ece_blobtree_table_t *table = _ece_blobtree_data_const(tree);

	struct avl_node *node = avl_find(&table->_entries, name);
	if (!node) {
		errno = ENOENT;
		return NULL;
	}

	_ece_blobtree_table_header_t *entry = container_of(node, _ece_blobtree_table_header_t, _node);

	errno = 0;
	return _ece_blobtree_unheader(entry);
}

bool ece_blobtree_table_insert_property(ece_blobtree_t *tree, const char *name, ece_blobtree_t *value) {
	assert(tree->_type == BLOBMSG_TYPE_TABLE);

	_ece_blobtree_table_t *table = _ece_blobtree_data(tree);

	size_t namelen = strlen(name);
	if (!_ece_blobtree_adjust_header(_ece_blobtree_table_header_len(namelen), &value))
		return false;

	ece_blobtree_table_delete_property(tree, name);

	_ece_blobtree_table_header_t *header = _ece_blobtree_header(value);
	memcpy(header->_name, name, namelen+1);
	header->_node.key = header->_name;
	_ece_blobtree_assert(avl_insert(&table->_entries, &header->_node) == 0);

	errno = 0;
	return true;
}

ece_blobtree_t * ece_blobtree_table_remove_property(ece_blobtree_t *tree, const char *name) {
	assert(tree->_type == BLOBMSG_TYPE_TABLE);

	_ece_blobtree_table_t *table = _ece_blobtree_data(tree);

	ece_blobtree_t *entry = ece_blobtree_table_get_property(tree, name);
	if (!entry)
		return NULL;

	_ece_blobtree_table_header_t *header = _ece_blobtree_header(entry);
	avl_delete(&table->_entries, &header->_node);

	return entry;
}

void ece_blobtree_table_delete_property(ece_blobtree_t *tree, const char *name) {
	ece_blobtree_t *entry = ece_blobtree_table_remove_property(tree, name);
	ece_blobtree_free(entry);
}

bool ece_blobtree_put(struct blob_buf *b, const ece_blobtree_t *tree, const char *name) {
	int type = tree->_type;

	if (type == BLOBMSG_TYPE_ARRAY) {
		void *c = blobmsg_open_array(b, name);
		if (!c)
			return false;

		ece_blobtree_t *entry;
		ece_blobtree_array_for_each(tree, entry) {
			if (!ece_blobtree_put(b, entry, NULL))
				return false;
		}

		blobmsg_close_array(b, c);

		return true;
	}
	else if (type == BLOBMSG_TYPE_TABLE) {
		void *c = blobmsg_open_table(b, name);
		if (!c)
			return false;

		const char *name;
		ece_blobtree_t *entry;
		ece_blobtree_table_for_each(tree, name, entry) {
			if (!ece_blobtree_put(b, entry, name))
				return false;
		}

		blobmsg_close_table(b, c);

		return true;
	}
	else {
		return !blobmsg_add_field(b, type, name, _ece_blobtree_data_const(tree), tree->_length);
	}
}

void ece_blobtree_free(ece_blobtree_t *tree) {
	if (!tree)
		return;

	int type = tree->_type;

	if (type == BLOBMSG_TYPE_ARRAY) {
		_ece_blobtree_array_t *array = _ece_blobtree_data(tree);
		_ece_blobtree_array_header_t *header, *next;
		list_for_each_entry_safe(header, next, &array->_list, _list)
			ece_blobtree_free(_ece_blobtree_unheader(header));
	}
	else if (type == BLOBMSG_TYPE_TABLE) {
		_ece_blobtree_table_t *table = _ece_blobtree_data(tree);
		_ece_blobtree_table_header_t *header, *next;
		avl_remove_all_elements(&table->_entries, header, _node, next)
			ece_blobtree_free(_ece_blobtree_unheader(header));
	}

	free(tree);
}
