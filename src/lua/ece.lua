--[[
  Copyright (c) 2016, Matthias Schiffer <mschiffer@universe-factory.net>
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice,
       this list of conditions and the following disclaimer.
    2. Redistributions in binary form must reproduce the above copyright notice,
       this list of conditions and the following disclaimer in the documentation
       and/or other materials provided with the distribution.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
]]


local ubus = require 'ubus'


module('ece', package.seeall)

local o = {}
local mt = {
	__index = o
}


function o:get(path)
	assert(getmetatable(self) == mt)

	local ret, status = self.ubus:call("ece", "get", { path = path })
	assert(status == nil)

	return ret.value
end

function o:set(path, value)
	assert(getmetatable(self) == mt)
	assert(value)

	local ret, status = self.ubus:call("ece", "set", { path = path, value = value })
	assert(status == nil)
end

function o:delete(path)
	assert(getmetatable(self) == mt)

	local ret, status = self.ubus:call("ece", "set", { path = path })
	assert(status == nil)
end

function o:insert(path, values)
	assert(getmetatable(self) == mt)
	assert(values)

	local ret, status = self.ubus:call("ece", "set", { path = path, insert = values })
	assert(status == nil)
end

function o:extend(path, prepend, append)
	assert(getmetatable(self) == mt)
	assert(prepend or append)

	local ret, status = self.ubus:call("ece", "set", { path = path, prepend = prepend, append = append })
	assert(status == nil)
end

function o:prepend(path, values)
	self:extend(path, values, nil)
end

function o:append(path, values)
	self:extend(path, nil, values)
end

function o:reset(path)
	assert(getmetatable(self) == mt)

	local ret, status = self.ubus:call("ece", "reset", { path = path })
	assert(status == nil)
end


function new()
	local conn = assert(ubus.connect(), 'Failed to connect to ubus')
	local ret = { ubus = conn }
	setmetatable(ret, mt)
	return ret
end
