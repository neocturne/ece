/*
  Copyright (c) 2016, Matthias Schiffer <mschiffer@universe-factory.net>
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice,
       this list of conditions and the following disclaimer.
    2. Redistributions in binary form must reproduce the above copyright notice,
       this list of conditions and the following disclaimer in the documentation
       and/or other materials provided with the distribution.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

/**
  \file

  json2blob utility
*/


#include "../../lib/blobstore/blobstore.h"

#include <libubox/blobmsg_json.h>
#include <json-c/json.h>

#include <errno.h>
#include <stdio.h>


/** The main function of json2blob */
int main(int argc, char *argv[])
{
	if (argc != 3) {
		fprintf(stderr, "Usage: json2blob <input> <output>\n");
		return 1;
	}

	struct blob_buf b = {};
	blob_buf_init(&b, 0);

	errno = 0;
	struct json_object *obj = json_object_from_file(argv[1]);
	if (!obj && errno)
		return 1;

	if (!blobmsg_add_json_element(&b, NULL, obj))
		return 1;

	json_object_put(obj);

	if (!ece_blob_store(argv[2], blob_data(b.head)))
		return 1;

	blob_buf_free(&b);
	return 0;
}
