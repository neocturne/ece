/*
  Copyright (c) 2016, Matthias Schiffer <mschiffer@universe-factory.net>
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice,
       this list of conditions and the following disclaimer.
    2. Redistributions in binary form must reproduce the above copyright notice,
       this list of conditions and the following disclaimer in the documentation
       and/or other materials provided with the distribution.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

/**
  \file

  Schema validation and lookup functions
*/


#include "schema.h"


const eced_schema_t * eced_schema_lookup_property(const eced_schema_t *schema, const char *key) {
	if (!(schema->type & ECED_TYPE_OBJECT))
		return NULL;

	if (schema->n_properties) {
		const eced_schema_property_t skey = {
			.key = (char *)key,
			.schema = NULL,
		};
		const eced_schema_property_t *prop = bsearch(
			&skey, schema->properties, schema->n_properties,
			sizeof(*schema->properties), eced_schema_cmp_properties
		);
		if (prop)
			return prop->schema;
	}

	return schema->additionalProperties;
}

const eced_schema_t * eced_schema_lookup_item(const eced_schema_t *schema, size_t index) {
	if (!(schema->type & ECED_TYPE_ARRAY))
		return NULL;

	if (index < schema->n_tuple_items)
		return schema->tuple_items[index];
	else
		return schema->list_items;
}

ssize_t eced_schema_get_property_required_index(const eced_schema_t *schema, const char *key) {
	char **req = bsearch(
		&key, schema->required, schema->n_required, sizeof(*schema->required),
		eced_schema_cmp_charpp
	);

	if (!req)
		return -1;

	return req - schema->required;
}

/** Validates that the given schema allows a "null" value */
static bool eced_schema_validate_null(const eced_schema_t *schema) {
	return schema->type & ECED_TYPE_NULL;
}

/** Validates a boolean value against a given schema */
static bool eced_schema_validate_boolean(const eced_schema_t *schema) {
	if (!(schema->type & ECED_TYPE_BOOLEAN))
		return false;

	return true;
}

/** Validates an int value against a given schema */
static bool eced_schema_validate_int(const eced_schema_t *schema, int32_t value) {
	if (!(schema->type & ECED_TYPE_INTEGER))
		return false;

	(void)value;

	return true;
}

/** Validates a string value against a given schema */
static bool eced_schema_validate_string(const eced_schema_t *schema, const char *value) {
	if (!(schema->type & ECED_TYPE_STRING))
		return false;

	(void)value;

	return true;
}

/** Validates an array value against a given schema */
static bool eced_schema_validate_array(const eced_schema_t *schema, const ece_blobtree_t *tree) {
	if (!(schema->type & ECED_TYPE_ARRAY))
		return false;

	size_t i = 0;
	ece_blobtree_t *item;
	ece_blobtree_array_for_each(tree, item) {
		const eced_schema_t *item_schema = eced_schema_lookup_item(schema, i++);
		if (!eced_schema_validate(item_schema, item))
			return false;
	}

	return true;
}

/** Validates an object value against a given schema */
static bool eced_schema_validate_object(const eced_schema_t *schema, const ece_blobtree_t *tree) {
	if (!(schema->type & ECED_TYPE_OBJECT))
		return false;

	bool found[schema->n_required];
	memset(found, 0, sizeof(found));


	const char *name;
	ece_blobtree_t *prop;
	ece_blobtree_table_for_each(tree, name, prop) {
		const eced_schema_t *property_schema = eced_schema_lookup_property(schema, name);
		if (!eced_schema_validate(property_schema, prop))
			return false;

		ssize_t index = eced_schema_get_property_required_index(schema, name);
		if (index >= 0)
			found[index] = true;
	}

	size_t i;
	for (i = 0; i < schema->n_required; i++) {
		if (!found[i])
			return false;
	}

	return true;
}

bool eced_schema_validate(const eced_schema_t *schema, const ece_blobtree_t *tree) {
	if (!schema)
		return false;

	switch (ece_blobtree_type(tree)) {
	case BLOBMSG_TYPE_UNSPEC:
		return eced_schema_validate_null(schema);

	case BLOBMSG_TYPE_BOOL:
		return eced_schema_validate_boolean(schema);

	case BLOBMSG_TYPE_INT32:
		return eced_schema_validate_int(schema, ece_blobtree_get_u32(tree));

	case BLOBMSG_TYPE_STRING:
		return eced_schema_validate_string(schema, ece_blobtree_get_string(tree));

	case BLOBMSG_TYPE_ARRAY:
		return eced_schema_validate_array(schema, tree);

	case BLOBMSG_TYPE_TABLE:
		return eced_schema_validate_object(schema, tree);

	default:
		return false;
	}
}

bool eced_schema_validate_delete_property(const eced_schema_t *schema, const char *key) {
	return eced_schema_get_property_required_index(schema, key) < 0;
}

bool eced_schema_validate_delete_item(const eced_schema_t *schema, const ece_blobtree_t *tree, size_t index) {
	if (ece_blobtree_type(tree) != BLOBMSG_TYPE_ARRAY)
		return false;

	if (index >= ece_blobtree_array_length(tree))
		return false;

	if (index+1 == ece_blobtree_array_length(tree))
		return true;

	return (index >= schema->n_tuple_items);
}

bool eced_schema_validate_insert_items(const eced_schema_t *schema, const ece_blobtree_t *tree, size_t index, const ece_blobtree_t *items) {
	if (ece_blobtree_type(tree) != BLOBMSG_TYPE_ARRAY || ece_blobtree_type(items) != BLOBMSG_TYPE_ARRAY)
		return false;

	if (index >= ece_blobtree_array_length(tree))
		return false;

	if (index < schema->n_tuple_items && index+1 != ece_blobtree_array_length(tree))
		return false;

	size_t i = index;
	ece_blobtree_t *item;
	ece_blobtree_array_for_each(items, item) {
		const eced_schema_t *item_schema = eced_schema_lookup_item(schema, i++);
		if (!eced_schema_validate(item_schema, item))
			return false;
	}

	return true;
}

bool eced_schema_validate_extend_items(const eced_schema_t *schema, const ece_blobtree_t *tree, const ece_blobtree_t *prepend, const ece_blobtree_t *append) {
	if (ece_blobtree_type(tree) != BLOBMSG_TYPE_ARRAY
		|| (prepend && ece_blobtree_type(prepend) != BLOBMSG_TYPE_ARRAY)
		|| (append && ece_blobtree_type(append) != BLOBMSG_TYPE_ARRAY)
	)
		return false;

	size_t len = ece_blobtree_array_length(tree);

	if (prepend) {
		if (schema->n_tuple_items)
			return false;

		ece_blobtree_t *item;
		ece_blobtree_array_for_each(prepend, item) {
			if (!eced_schema_validate(schema->list_items, item))
				return false;
		}

		len += ece_blobtree_array_length(prepend);
	}

	if (append) {
		size_t i = len;
		ece_blobtree_t *item;
		ece_blobtree_array_for_each(append, item) {
			const eced_schema_t *item_schema = eced_schema_lookup_item(schema, i++);
			if (!eced_schema_validate(item_schema, item))
				return false;
		}
	}

	return true;
}
