/*
  Copyright (c) 2016, Matthias Schiffer <mschiffer@universe-factory.net>
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice,
       this list of conditions and the following disclaimer.
    2. Redistributions in binary form must reproduce the above copyright notice,
       this list of conditions and the following disclaimer in the documentation
       and/or other materials provided with the distribution.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

/**
  \file

  Transformation of invalid configuration elements
*/


#include "schema.h"
#include "../util.h"

#include <libubox/ulog.h>

#include <string.h>


/**
  Escapes a JSON Pointer path component

  The output buffer must hold at least twice as many characters as the
  input string (plus zero termination).
*/
static void eced_schema_escape_path(char *out, const char *in) {
	do {
		switch (*in) {
		case '~':
			*out++ = '~';
			*out++ = '0';
			break;

		case '/':
			*out++ = '~';
			*out++ = '1';
			break;

		default:
			*out++ = *in;
		}
	} while (*in++);
}

/** Generates a blobtree of the default value for a given schema, rooted at the given path */
ece_blobtree_t * eced_schema_get_default_at(const char *path, const eced_schema_t *schema) {
	ece_blobtree_t *ret = NULL;
	if (schema->default_value) {
		ret = ece_blobtree_clone(schema->default_value);
	}
	else if (schema->type & ECED_TYPE_NULL) {
		ret = ece_blobtree_new_empty();
	}
	else if (schema->type & ECED_TYPE_BOOLEAN) {
		ret = ece_blobtree_new_bool(false);
	}
	else if (schema->type & ECED_TYPE_INTEGER) {
		ret = ece_blobtree_new_u32(0);
	}
	else if (schema->type & ECED_TYPE_STRING) {
		ret = ece_blobtree_new_string("");
	}
	else if (schema->type & ECED_TYPE_ARRAY) {
		ret = ece_blobtree_new_array();
	}
	else if (schema->type & ECED_TYPE_OBJECT) {
		ret = ece_blobtree_new_table();

		size_t i;
		for (i = 0; i < schema->n_properties; i++) {
			eced_schema_property_t *prop = &schema->properties[i];
			eced_schema_t *prop_schema = prop->schema;

			if (!prop_schema->default_value)
				continue;

			ece_blobtree_t *def = ece_blobtree_clone(prop_schema->default_value);

			if (!def || !ece_blobtree_table_insert_property(ret, prop->key, def)) {
				ece_blobtree_free(def);
				ece_blobtree_free(ret);
				return NULL;
			}
		}
	}

	if (!ret || !eced_schema_validify_at(path, schema, ret, NULL, NULL)) {
		ulog(LOG_ERR, "required schema field '%s' without valid default value\n", path);
		ece_blobtree_free(ret);
		return NULL;
	}

	return ret;
}

bool eced_schema_has_explicit_default(const eced_schema_t *schema) {
	return schema->default_value;
}

/**
  Transforms the given object into a valid one

  Invalid properties are removed, required properties set to default values.
*/
static bool eced_schema_validify_object_at(const char *path, const eced_schema_t *schema, ece_blobtree_t *tree, eced_schema_validify_cb_t cb, void *arg) {
	if (!schema || !(schema->type & ECED_TYPE_OBJECT))
		return false;

	size_t pathlen = strlen(path);

	const char *name;
	ece_blobtree_t *prop, *next;
	ece_blobtree_table_for_each_safe(tree, name, prop, next) {
		const eced_schema_t *propschema = eced_schema_lookup_property(schema, name);

		size_t proplen = strlen(name);
		char proppath[pathlen + 1 + 2*proplen + 1];
		memcpy(proppath, path, pathlen);
		proppath[pathlen] = '/';
		eced_schema_escape_path(proppath+pathlen+1, name);

		if (!eced_schema_validify_at(proppath, propschema, prop, cb, arg)) {
			ulog(LOG_DEBUG, "invalid property at '%s', removing\n", proppath);
			prop = ece_blobtree_table_remove_property(tree, name);

			if (cb)
				cb(proppath, prop, arg);
			else
				ece_blobtree_free(prop);
		}
	}

	size_t i;
	for (i = 0; i < schema->n_required; i++) {
		const char *name = schema->required[i];
		if (ece_blobtree_table_get_property(tree, name))
			continue;

		const eced_schema_t *propschema = eced_schema_lookup_property(schema, name);
		if (!propschema) {
			ulog(LOG_WARNING, "invalid required property in schema\n");
			continue;
		}

		size_t proplen = strlen(name);
		char proppath[pathlen + 1 + 2*proplen + 1];
		memcpy(proppath, path, pathlen);
		proppath[pathlen] = '/';
		eced_schema_escape_path(proppath+pathlen+1, name);

		ulog(LOG_DEBUG, "missing property at '%s', fixing up\n", proppath);

		if (cb)
			cb(proppath, NULL, arg);

		prop = eced_schema_get_default_at(proppath, propschema);
		if (!prop)
			continue; /* Error message by eced_schema_get_default() */

		ece_blobtree_table_insert_property(tree, name, prop);
	}

	return true;
}

bool eced_schema_validify_at(const char *path, const eced_schema_t *schema, ece_blobtree_t *tree, eced_schema_validify_cb_t cb, void *arg) {
	switch (ece_blobtree_type(tree)) {
	case BLOBMSG_TYPE_TABLE:
		return eced_schema_validify_object_at(path, schema, tree, cb, arg);

	/* TODO: case BLOBMSG_TYPE_ARRAY: */

	default:
		return eced_schema_validate(schema, tree);
	}
}
