/*
  Copyright (c) 2016, Matthias Schiffer <mschiffer@universe-factory.net>
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice,
       this list of conditions and the following disclaimer.
    2. Redistributions in binary form must reproduce the above copyright notice,
       this list of conditions and the following disclaimer in the documentation
       and/or other materials provided with the distribution.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

/**
  \file

  ECE schema merging
*/


#include "schema.h"

#include <libubox/ulog.h>

#include <assert.h>
#include <string.h>


/** Creates a copy of an ECE schema */
static eced_schema_t * eced_schema_clone(const eced_schema_t *schema) {
	size_t i;

	eced_schema_t *ret = calloc(1, sizeof(*ret));
	if (!ret)
		return NULL;

	ret->type = schema->type;

	ret->n_properties = schema->n_properties;
	ret->properties = calloc(ret->n_properties, sizeof(*ret->properties));
	if (!ret->properties)
		goto err;

	for (i = 0; i < schema->n_properties; i++) {
		const eced_schema_property_t *prop = &schema->properties[i];
		eced_schema_property_t *ret_prop = &ret->properties[i];

		ret_prop->key = strdup(prop->key);
		ret_prop->schema = eced_schema_clone(prop->schema);
		if (!ret_prop->key || !ret_prop->schema)
			goto err;
	}

	if (schema->additionalProperties) {
		ret->additionalProperties = eced_schema_clone(schema->additionalProperties);
		if (!ret->additionalProperties)
			goto err;
	}

	ret->n_required = schema->n_required;
	ret->required = calloc(ret->n_required, sizeof(*ret->required));
	if (!ret->required)
		goto err;

	for (i = 0; i < schema->n_required; i++) {
		ret->required[i] = strdup(schema->required[i]);
		if (!ret->required[i])
			goto err;
	}

	ret->n_tuple_items = schema->n_tuple_items;
	ret->tuple_items = calloc(ret->n_tuple_items, sizeof(*ret->tuple_items));
	if (!ret->tuple_items)
		goto err;

	for (i = 0; i < schema->n_tuple_items; i++) {
		const eced_schema_item_t item = schema->tuple_items[i];
		eced_schema_item_t *ret_item = &ret->tuple_items[i];

		*ret_item = eced_schema_clone(item);
		if (!*ret_item)
			goto err;
	}

	if (schema->list_items) {
		ret->list_items = eced_schema_clone(schema->list_items);
		if (!ret->list_items)
			goto err;
	}

	if (schema->default_value) {
		ret->default_value = ece_blobtree_clone(schema->default_value);
		if (!ret->default_value)
			goto err;
	}

	return ret;

err:
	eced_schema_free(ret);
	return NULL;
}

eced_schema_t * eced_schema_merge(const eced_schema_t *schema1, const eced_schema_t *schema2) {
	eced_schema_t *merged = calloc(1, sizeof(*merged));
	if (!merged)
		return NULL;

	merged->type = schema1->type & schema2->type;

	if (merged->type & ECED_TYPE_OBJECT) {
		merged->n_properties = schema1->n_properties;

		size_t i;
		for (i = 0; i < schema2->n_properties; i++) {
			if (eced_schema_lookup_property(schema1, schema2->properties[i].key))
				continue;

			merged->n_properties++;
		}

		merged->properties = calloc(merged->n_properties, sizeof(*merged->properties));
		if (!merged->properties)
			goto err;

		size_t j = 0;
		for (i = 0; i < schema1->n_properties; i++) {
			eced_schema_property_t *entry = &schema1->properties[i];

			const eced_schema_t *prop1 = entry->schema;
			const eced_schema_t *prop2 = eced_schema_lookup_property(schema2, entry->key);

			eced_schema_property_t *merged_entry = &merged->properties[j++];
			merged_entry->key = strdup(entry->key);

			if (prop2)
				merged_entry->schema = eced_schema_merge(prop1, prop2);
			else
				merged_entry->schema = eced_schema_clone(prop1);

			if (!merged_entry->key || !merged_entry->schema)
				goto err;
		}

		for (i = 0; i < schema2->n_properties; i++) {
			if (eced_schema_lookup_property(schema1, schema2->properties[i].key))
				continue;

			eced_schema_property_t *entry = &schema2->properties[i];
			const eced_schema_t *prop = entry->schema;

			eced_schema_property_t *merged_entry = &merged->properties[j++];
			merged_entry->key = strdup(entry->key);
			merged_entry->schema = eced_schema_clone(prop);

			if (!merged_entry->key || !merged_entry->schema)
				goto err;
		}

		assert(j == merged->n_properties);

		qsort(merged->properties, merged->n_properties, sizeof(*merged->properties), eced_schema_cmp_properties);

		if (schema1->additionalProperties && schema2->additionalProperties) {
			merged->additionalProperties = eced_schema_merge(schema1->additionalProperties, schema2->additionalProperties);
			if (!merged->additionalProperties)
				goto err;
		}

		merged->n_required = schema1->n_required;

		for (i = 0; i < schema2->n_required; i++) {
			if (eced_schema_get_property_required_index(schema1, schema2->required[i]) >= 0)
				continue;

			merged->n_required++;
		}

		merged->required = calloc(merged->n_required, sizeof(*merged->required));
		if (!merged->required)
			goto err;

		j = 0;
		for (i = 0; i < schema1->n_required; i++) {
			char **merged_req = &merged->required[j++];
			*merged_req = strdup(schema1->required[i]);
			if (!*merged_req)
				goto err;
		}

		for (i = 0; i < schema2->n_required; i++) {
			if (eced_schema_get_property_required_index(schema1, schema2->required[i]) >= 0)
				continue;

			char **merged_req = &merged->required[j++];
			*merged_req = strdup(schema2->required[i]);

			if (!*merged_req)
				goto err;
		}

		assert(j == merged->n_required);

		qsort(merged->required, merged->n_required, sizeof(*merged->required), eced_schema_cmp_charpp);
	}

	if (merged->type & ECED_TYPE_ARRAY) {
		merged->n_tuple_items = (schema1->n_tuple_items > schema2->n_tuple_items) ? schema1->n_tuple_items : schema2->n_tuple_items;
		merged->tuple_items = calloc(merged->n_tuple_items, sizeof(*merged->tuple_items));
		if (!merged->tuple_items)
			goto err;

		size_t i;
		for (i = 0; i < merged->n_tuple_items; i++) {
			const eced_schema_t *item1 = eced_schema_lookup_item(schema1, i);
			const eced_schema_t *item2 = eced_schema_lookup_item(schema2, i);
			merged->tuple_items[i] = eced_schema_merge(item1, item2);
			if (!merged->tuple_items[i])
				goto err;
		}

		if (schema1->list_items && schema2->list_items) {
			merged->list_items = eced_schema_merge(schema1->list_items, schema2->list_items);
			if (!merged->list_items)
				goto err;
		}
	}

	if (schema2->default_value && eced_schema_validate(merged, schema2->default_value)) {
		merged->default_value = ece_blobtree_clone(schema2->default_value);
	}
	else if (schema1->default_value && eced_schema_validate(merged, schema1->default_value)) {
		ulog(LOG_WARNING, "invalid default encountered in schema merging, falling back to old default\n");
		merged->default_value = ece_blobtree_clone(schema1->default_value);
	}
	else if (schema1->default_value || schema2->default_value) {
		ulog(LOG_WARNING, "both defaults invalid after schema merging\n");
	}

	return merged;

err:
	eced_schema_free(merged);
	return NULL;
}
