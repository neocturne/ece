/*
  Copyright (c) 2016, Matthias Schiffer <mschiffer@universe-factory.net>
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice,
       this list of conditions and the following disclaimer.
    2. Redistributions in binary form must reproduce the above copyright notice,
       this list of conditions and the following disclaimer in the documentation
       and/or other materials provided with the distribution.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

/**
  \file

  ECE schema parser and validator - private definitions
*/


#pragma once


#include "../schema.h"

#include <stddef.h>
#include <stdint.h>


/**
  Enumeration of all valid types in an ECE schema,
  as well as special values for no and all types
*/
typedef enum eced_type {
	ECED_TYPE_INVALID = 0,

	ECED_TYPE_ARRAY   = (1 << 0),
	ECED_TYPE_BOOLEAN = (1 << 1),
	ECED_TYPE_INTEGER = (1 << 2),
	ECED_TYPE_NULL    = (1 << 3),
	ECED_TYPE_OBJECT  = (1 << 4),
	ECED_TYPE_STRING  = (1 << 5),

	ECED_TYPE_ANY     =
		ECED_TYPE_ARRAY |
		ECED_TYPE_BOOLEAN |
		ECED_TYPE_INTEGER |
		ECED_TYPE_NULL |
		ECED_TYPE_OBJECT |
		ECED_TYPE_STRING,
} eced_type_t;

/** Describes a single property in the "properties" attribute of an object schema */
typedef struct eced_schema_property {
	char *key;             /**< The key (name) of the property */
	eced_schema_t *schema; /**< THe schema for the value of the property */
} eced_schema_property_t;

/** The schema of a tuple item */
typedef eced_schema_t *eced_schema_item_t;

/**
  An ECE schema

  Describes the valid values for a configuration item.
*/
struct eced_schema {
	uint8_t type;                        /**< Bitfield of all valid types of the configuration item */

	size_t n_properties;                 /**< Number of entries in the properties field */
	eced_schema_property_t *properties;  /**< Schemas of names properties of an object */

	eced_schema_t *additionalProperties; /**< Schema for properties not listed in the properties field */

	size_t n_required;                   /**< Number of entrires in the required field */
	char **required;                     /**< List of required properties */

	size_t n_tuple_items;                /**< Number of tuple items */
	eced_schema_item_t *tuple_items;     /**< Tuple-type items for array validation */

	eced_schema_t *list_items;           /**< Schema of list-type items */

	ece_blobtree_t *default_value;       /**< The default value of this schema */
};


/** Comparator for eced_schema_property_t values */
int eced_schema_cmp_properties(const void *a, const void *b);

/** Comparator for char ** values */
int eced_schema_cmp_charpp(const void *a, const void *b);

/**
  Returns the index of a property in the list of required keys of a object

  Returns -1 if the key doesn't exist.
*/
ssize_t eced_schema_get_property_required_index(const eced_schema_t *schema, const char *key);
