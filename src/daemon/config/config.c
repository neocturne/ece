/*
  Copyright (c) 2016, Matthias Schiffer <mschiffer@universe-factory.net>
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice,
       this list of conditions and the following disclaimer.
    2. Redistributions in binary form must reproduce the above copyright notice,
       this list of conditions and the following disclaimer in the documentation
       and/or other materials provided with the distribution.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

/**
  \file

  Functions to load and access configuration and schema - implementation
*/



#include "config.h"
#include "../../lib/blobstore/blobstore.h"

#include <libubox/list.h>
#include <libubox/ulog.h>
#include <libubox/uloop.h>

#include <sys/types.h>

#include <dirent.h>
#include <errno.h>
#include <string.h>

#include <sys/stat.h>


/** Determines after how many ms without config changes the config is committed */
#define ECED_COMMIT_TIMEOUT 5000


/** The loaded schema */
static eced_schema_t *config_schema = NULL;
/** The configuration data */
static ece_blobtree_t *config = NULL;

static const char *schema_dir = NULL;
/** Path of the file diffs are loaded from and stored into */
static const char *diff_file = NULL;


/** True if there is currently a config commit scheduled */
bool dirty = false;



bool eced_config_unescape_path(char *path) {
	const char *in = path;
	char *out = path;

	do {
		if (in[0] == '~') {
			switch (in[1]) {
			case '0':
				*out++ = '~';
				break;

			case '1':
				*out++ = '/';
				break;

			default:
				return false;
			}

			in++;
		}
		else {
			*out++ = in[0];
		}
	} while (*in++);

	return true;
}

eced_config_pointer_t eced_config_root(void) {
	return (eced_config_pointer_t){
		.schema = config_schema,
		.object = config,
		.parent_schema = NULL,
		.parent = NULL,
		.last = NULL,
	};
}

/** Callback for scheduled config commits */
static void eced_config_commit_cb(UNUSED struct uloop_timeout *t) {
	eced_config_commit();
}

/** Timeout to schedule config commits */
static struct uloop_timeout commit_timeout = {
	.cb = eced_config_commit_cb
};

void eced_config_schedule_commit(void) {
	dirty = true;
	uloop_timeout_set(&commit_timeout, ECED_COMMIT_TIMEOUT);
}

bool eced_config_commit(void) {
	uloop_timeout_cancel(&commit_timeout);

	if (!dirty)
		return true;

	dirty = false;

	if (!eced_config_store_diff(diff_file)) {
		ulog(LOG_WARNING, "config commit failed\n");
		return false;
	}

	ulog(LOG_DEBUG, "config committed\n");
	return true;
}

/** Loads a schema and merges it into the currently loaded one */
static bool eced_config_load_schema(eced_schema_t **schema, const char *schema_filename) {
	eced_schema_t *new_schema = eced_schema_load(schema_filename);
	if (!new_schema) {
		ulog(LOG_ERR, "failed to load schema '%s'\n", schema_filename);
		return false;
	}

	eced_schema_t *merged = eced_schema_merge(*schema, new_schema);
	eced_schema_free(new_schema);

	if (!merged) {
		ulog(LOG_ERR, "failed to merge schema '%s'\n", schema_filename);
		return false;
	}

	eced_schema_free(*schema);
	*schema = merged;

	ulog(LOG_DEBUG, "loaded schema '%s'\n", schema_filename);

	return true;
}

static eced_schema_t * eced_config_load_schemas() {
	typedef struct schema_entry {
		struct schema_entry *next;
		char *path;
	} schema_entry_t;
	schema_entry_t *entry = NULL;

	eced_schema_t *schema = NULL;

	DIR *dir = opendir(schema_dir);
	if (!dir) {
		ulog(LOG_ERR, "unable to open schema directory\n");
		return false;
	}

	size_t dir_len = strlen(schema_dir);
	if (schema_dir[dir_len-1] == '/')
		dir_len--;

	struct dirent *ent;
	while ((ent = readdir(dir))) {
		if (ent->d_name[0] == '.')
			continue;

		size_t file_len = strlen(ent->d_name);
		char path[dir_len + 1 + file_len + 1];
		memcpy(path, schema_dir, dir_len);
		path[dir_len] = '/';
		memcpy(path+dir_len+1, ent->d_name, file_len+1);

		struct stat statbuf;
		if (stat(path, &statbuf)) {
			ulog(LOG_WARNING, "ignoring file '%s': stat failed: %s", ent->d_name, strerror(errno));
			continue;
		}
		if ((statbuf.st_mode & S_IFMT) != S_IFREG) {
			ulog(LOG_INFO, "ignoring file '%s': no regular file", ent->d_name);
			continue;
		}

		schema_entry_t *new_entry = malloc(sizeof(*new_entry));
		if (!new_entry)
			goto out;

		new_entry->path = strdup(path);
		if (!new_entry->path) {
			free(new_entry);
			goto out;
		}

		schema_entry_t **pos;
		for (pos = &entry; *pos; pos = &(*pos)->next) {
			if (strcmp((*pos)->path, new_entry->path) > 0)
				break;
		}

		new_entry->next = *pos;
		*pos = new_entry;
	}

	closedir(dir);

	schema = eced_schema_create_empty();
	if (!schema) {
		ulog(LOG_ERR, "failed to allocate empty schema\n");
		return NULL;
	}

out:
	while (entry) {
		schema_entry_t *next = entry->next;

		if (schema)
			eced_config_load_schema(&schema, entry->path);

		free(entry->path);
		free(entry);
		entry = next;
	}

	return schema;
}

static bool eced_config_load(void) {
	eced_config_free();

	eced_schema_t *schema = eced_config_load_schemas();
	if (!schema) {
		ulog(LOG_ERR, "failed to load schemas\n");
		return false;
	}

	/* DEBUG */
	eced_schema_dump(schema);

	ece_blobtree_t *new_config = eced_schema_get_default_at("", schema);
	if (!new_config) {
		ulog(LOG_ERR, "failed to generate default configuration: %s\n", strerror(errno));
		eced_schema_free(config_schema);
		return false;
	}

	eced_schema_free(config_schema);
	ece_blobtree_free(config);

	config_schema = schema;
	config = new_config;

	if (!eced_config_load_diff(diff_file)) {
		ulog(LOG_ERR, "unable to load diff file: %s\n", strerror(errno));
		exit(1);
	}

	return true;
}

bool eced_config_reload(void) {
	eced_config_commit();
	return eced_config_load();
}

bool eced_config_init(const char *schema_directory, const char *diff_filename) {
	schema_dir = schema_directory;
	diff_file = diff_filename;

	return eced_config_load();
}

void eced_config_free(void) {
	ece_blobtree_free(config);
	config = NULL;

	eced_schema_free(config_schema);
	config_schema = NULL;

	eced_config_free_diff();
}

eced_config_pointer_t eced_config_lookup_path(eced_config_pointer_t root, const char *path, const char **rest, unsigned flags) {
	eced_config_pointer_t p = root;

	const char *prev = NULL;

	if (path[0] && path[0] != '/')
		goto invalid;

	while (p.schema && path[0]) {
		prev = path;

		path = strchrnul(path+1, '/');

		size_t len = path-(prev+1);
		char frag[len+1];
		memcpy(frag, prev+1, len);
		frag[len] = 0;

		if (!eced_config_unescape_path(frag))
			goto invalid;

		int type = ece_blobtree_type(p.object);
		if (type == BLOBMSG_TYPE_ARRAY && (flags&ECED_LOOKUP_F_NO_ARRAYS)) {
			path = prev;
			break;
		}

		p.parent = p.object;
		p.parent_schema = p.schema;

		if (type == BLOBMSG_TYPE_TABLE) {
			p.schema = eced_schema_lookup_property(p.parent_schema, frag);
			p.object = ece_blobtree_table_get_property(p.parent, frag);

			if (!p.object && (!(flags&ECED_LOOKUP_F_ALLOW_MISSING) || path[0]))
				goto notfound;

		}
		else if (type == BLOBMSG_TYPE_ARRAY) {
			char *end;
			size_t index = strtoul(frag, &end, 10);
			if (!frag[0] || end[0])
				goto notfound;

			p.schema = eced_schema_lookup_item(p.parent_schema, index);
			p.object = ece_blobtree_array_get_item(p.parent, index);

			if (!p.object)
				goto notfound;
		}
		else {
			goto invalid;
		}
	}

	if (!p.schema)
		goto notfound;

	if (rest)
		*rest = path;
	if (prev)
		p.last = prev + 1;

	errno = 0;
	return p;

invalid:
	errno = EINVAL;
	return ECED_CONFIG_NULL;

notfound:
	errno = ENOENT;
	return ECED_CONFIG_NULL;
}
