/*
  Copyright (c) 2016, Matthias Schiffer <mschiffer@universe-factory.net>
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice,
       this list of conditions and the following disclaimer.
    2. Redistributions in binary form must reproduce the above copyright notice,
       this list of conditions and the following disclaimer in the documentation
       and/or other materials provided with the distribution.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

/**
  \file

  Functions to modify the configuration
*/


#include "config.h"
#include "../util.h"
#include "../../lib/blobstore/blobstore.h"

#include <libubox/list.h>
#include <libubox/ulog.h>

#include <assert.h>
#include <errno.h>
#include <fcntl.h>
#include <stdio.h>
#include <unistd.h>

#include <sys/stat.h>


/** A diff entry describing a configuration change at a given path */
typedef struct ece_diff_entry {
	struct list_head list;  /**< List head */
	ece_blobtree_t *object; /**< The configuration to set at the given path */
	char path[];            /**< The path */
} eced_diff_entry_t;


/**
  List of paths for which diffs should be saved

  The config field of these diffs is unused.
*/
static LIST_HEAD(diffs);

/** List of invalid diffs which have been loaded, but couldn't be applied */
static LIST_HEAD(invalid_diffs);


/** Allocates a new eced_diff_entry_t containing a given path */
static eced_diff_entry_t * eced_config_alloc_diff_entry(const char *path, ece_blobtree_t *object) {
	size_t len = strlen(path) + 1;
	size_t alloc_len = (sizeof(eced_diff_entry_t) + len + 7) & ~((size_t)7);

	eced_diff_entry_t *ret = malloc(alloc_len);
	if (!ret)
		return NULL;

	ret->object = object;

	memcpy(ret->path, path, len);
	return ret;
}

/** Frees the given eced_diff_entry_t */
static void eced_config_free_diff_entry(eced_diff_entry_t *entry) {
	if (!entry)
		return;

	ece_blobtree_free(entry->object);
	free(entry);
}

/** Removes and frees the given eced_diff_entry_t */
static void eced_config_delete_diff_entry(eced_diff_entry_t *entry) {
	list_del(&entry->list);
	eced_config_free_diff_entry(entry);
}

/** Returns true if path a is a prefix of path b */
static inline bool eced_config_path_prefix_of(const char *a, const char *b) {
	size_t alen = strlen(a);
	if (strncmp(a, b, alen) != 0)
		return false;

	if (b[alen] != 0 && b[alen] != '/')
		return false;

	return true;
}

/** Adds a new diff entry (removing all redundant entries) */
static void eced_config_add_diff(eced_diff_entry_t *diff_entry) {
	const char *diff_root = diff_entry->path;

	eced_diff_entry_t *cur, *next;
	list_for_each_entry_safe(cur, next, &diffs, list) {
		if (eced_config_path_prefix_of(cur->path, diff_root)) {
			ulog(LOG_DEBUG, "not creating a new diff record for %s, covered by %s\n", diff_root, cur->path);
			eced_config_free_diff_entry(diff_entry);
			return;
		}

		if (eced_config_path_prefix_of(diff_root, cur->path)) {
			ulog(LOG_DEBUG, "replacing diff for %s with %s\n", cur->path, diff_root);
			eced_config_delete_diff_entry(cur);
		}
	}

	ulog(LOG_DEBUG, "adding diff for %s\n", diff_root);

	list_for_each_entry_safe(cur, next, &invalid_diffs, list) {
		if (eced_config_path_prefix_of(diff_root, cur->path)) {
			ulog(LOG_DEBUG, "removing invalid diff for %s\n", cur->path);
			eced_config_delete_diff_entry(cur);
		}
	}

	list_add_tail(&diff_entry->list, &diffs);
}

/** Adds a new entry to the list of invalid diffs */
static void eced_config_add_invalid_diff(eced_diff_entry_t *diff_entry) {
	const char *diff_root = diff_entry->path;

	eced_diff_entry_t *cur;
	list_for_each_entry(cur, &diffs, list) {
		if (strcmp(cur->path, diff_root) == 0) {
			ulog(LOG_DEBUG, "not creating a new invalid diff record for %s, path has valid record\n", diff_root);
			eced_config_free_diff_entry(diff_entry);
			return;
		}
	}

	list_for_each_entry(cur, &invalid_diffs, list) {
		if (eced_config_path_prefix_of(cur->path, diff_root)) {
			ulog(LOG_DEBUG, "not creating a new invalid diff record for %s, covered by %s\n", diff_root, cur->path);
			eced_config_free_diff_entry(diff_entry);
			return;
		}
	}

	ulog(LOG_DEBUG, "storing diff for %s as invalid\n", diff_root);

	list_add_tail(&diff_entry->list, &invalid_diffs);
}

/**
  Adds a list of entries entry to the list of invalid diffs

  The list will be in unspecified state after this operation and must be reinitialized
  before it may be reused.
*/
static void eced_config_add_invalid_diff_list(struct list_head *list) {
	eced_diff_entry_t *cur, *next;
	list_for_each_entry_safe(cur, next, list, list)
		eced_config_add_invalid_diff(cur);
}

static void eced_config_reset_diff(const char *path) {
	eced_diff_entry_t *cur, *next;
	list_for_each_entry_safe(cur, next, &diffs, list) {
		if (eced_config_path_prefix_of(path, cur->path)) {
			ulog(LOG_DEBUG, "removing diff for %s\n", cur->path);
			eced_config_delete_diff_entry(cur);
		}
	}

	list_for_each_entry_safe(cur, next, &invalid_diffs, list) {
		if (eced_config_path_prefix_of(path, cur->path)) {
			ulog(LOG_DEBUG, "removing invalid diff for %s\n", cur->path);
			eced_config_delete_diff_entry(cur);
		}
	}
}

/** Loads a single config diff records, integrating it into the config tree */
static bool eced_config_handle_diff_record(struct blob_attr *record) {
	enum {
		ATTR_PATH,
		ATTR_VALUE,
		ATTR_MAX
	};

	const struct blobmsg_policy policy[ATTR_MAX] = {
		[ATTR_PATH]    = { .name = "path", .type = BLOBMSG_TYPE_STRING },
		[ATTR_VALUE]   = { .name = "value", .type = BLOBMSG_TYPE_UNSPEC },
	};

	struct blob_attr *attr[ATTR_MAX];
	blobmsg_parse(policy, ATTR_MAX, attr, blobmsg_data(record), blobmsg_len(record));
	if (!attr[ATTR_PATH]) {
		ulog(LOG_ERR, "broken diff record\n");
		return false;
	}

	eced_config_modify(
		blobmsg_get_string(attr[ATTR_PATH]), attr[ATTR_VALUE], NULL, NULL, NULL, false, true, true);

	return true;
}

bool eced_config_load_diff(const char *filename) {
	bool ret = false;

	struct blob_attr *diff = ece_blob_load(filename);
	if (!diff) {
		ulog(LOG_ERR, "unable to load diff file '%s'\n", filename);
		return false;
	}

	if (blobmsg_type(diff) != BLOBMSG_TYPE_ARRAY || !blobmsg_check_attr_list(diff, BLOBMSG_TYPE_TABLE)) {
		ulog(LOG_ERR, "broken diff file\n");
		goto out;
	}

	struct blob_attr *cur;
	ssize_t rem;
	blobmsg_for_each_attr(cur, diff, rem) {
		if (!eced_config_handle_diff_record(cur) && errno == ENOMEM)
			goto out;
	}

	ret = true;

out:
	free(diff);
	return ret;
}

/** Writes a blobmsg table representing the given diff to a blob_buf */
static bool eced_config_put_diff(struct blob_buf *b, const char *path, ece_blobtree_t *config) {
	void *c = blobmsg_open_table(b, NULL);
	if (!c)
		return false;

	if (blobmsg_add_string(b, "path", path) != 0)
		return false;

	if (config) {
		if (!ece_blobtree_put(b, config, "value"))
			return false;
	}

	blobmsg_close_table(b, c);

	return true;
}

static void eced_config_sync_dir(const char *dirname) {
	int fd = open(dirname, O_RDONLY|O_DIRECTORY);
	if (fd < 0)
		goto err;

	if (fsync(fd)) {
		close(fd);
		goto err;
	}

	close(fd);
	return;

err:
	ulog(LOG_WARNING, "unable to sync directory '%s': %s\n", dirname, strerror(errno));
}

static bool eced_config_store_blob(const char *filename, const struct blob_attr *blob) {
	size_t filename_len = strlen(filename);
	char tmp_filename[filename_len + 5 + 1]; /* ".tmp." */

	const char *slash = strrchr(filename, '/');
	size_t name_index = 0;
	if (slash) {
		name_index = (slash-filename)+1;
		memcpy(tmp_filename, filename, name_index);
	}

	memcpy(tmp_filename+name_index, ".tmp.", 5);
	memcpy(tmp_filename+name_index+5, filename+name_index, filename_len-name_index+1);

	if (!ece_blob_store(tmp_filename, blob))
		return false;

	if (rename(tmp_filename, filename))
		return false;

	tmp_filename[name_index] = 0;
	eced_config_sync_dir(tmp_filename);

	return true;
}

bool eced_config_store_diff(const char *filename) {
	bool ret = false;

	struct blob_buf b = {};
	blob_buf_init(&b, 0);

	void *array = blobmsg_open_array(&b, NULL);
	if (!array)
		goto out;

	eced_diff_entry_t *diff;
	list_for_each_entry(diff, &diffs, list) {
		eced_config_pointer_t p = eced_config_lookup_path(eced_config_root(), diff->path, NULL, ECED_LOOKUP_F_ALLOW_MISSING);
		assert(p.schema);

		if (!eced_config_put_diff(&b, diff->path, p.object))
			goto out;
	}

	list_for_each_entry(diff, &invalid_diffs, list) {
		if (!eced_config_put_diff(&b, diff->path, diff->object))
			goto out;
	}

	blobmsg_close_array(&b, array);

	ret = eced_config_store_blob(filename, blob_data(b.head));

out:
	blob_buf_free(&b);
	return ret;
}

/**
  Frees all entries in the given list of eced_diff_entry_t

  The list will be empty after this operation.
*/
static void eced_config_free_diff_list(struct list_head *list) {
	eced_diff_entry_t *cur, *next;
	list_for_each_entry_safe(cur, next, list, list)
		eced_config_delete_diff_entry(cur);
}

void eced_config_free_diff(void) {
	eced_config_free_diff_list(&diffs);
	eced_config_free_diff_list(&invalid_diffs);
}

/** Handler for invalid entries encountered by eced_schema_validify_at */
static void eced_config_validify_handler(const char *path, ece_blobtree_t *value, void *arg) {
	struct list_head *validify_diffs = arg;

	eced_diff_entry_t *invalid_diff = eced_config_alloc_diff_entry(path, value);
	if (!invalid_diff) {
		ece_blobtree_free(value);
		return;
	}

	list_add_tail(&invalid_diff->list, validify_diffs);
}

/** Sets the config described by the given pointer (if valid) */
static bool eced_config_set(
	eced_config_pointer_t p, const char *last, const struct blob_attr *value,
	const char *validify_path, bool store_invalid, struct list_head *validify_diffs)
{
	ece_blobtree_t *tree = ece_blobtree_parse(value);
	if (!tree)
		return false;

	bool valid;
	if (validify_path && store_invalid)
		valid = eced_schema_validify_at(
			validify_path, p.schema, tree, eced_config_validify_handler, validify_diffs);
	else if (validify_path)
		valid = eced_schema_validify_at(validify_path, p.schema, tree, NULL, NULL);
	else
		valid = eced_schema_validate(p.schema, tree);

	if (!valid) {
		eced_diff_entry_t *invalid_diff = NULL;
		if (validify_path && store_invalid)
			invalid_diff = eced_config_alloc_diff_entry(validify_path, tree);

		if (invalid_diff)
			eced_config_add_invalid_diff(invalid_diff);
		else
			ece_blobtree_free(tree);

		errno = EINVAL;
		return false;
	}

	if (ece_blobtree_type(p.parent) == BLOBMSG_TYPE_TABLE) {
		if (!ece_blobtree_table_insert_property(p.parent, last, tree)) {
			ece_blobtree_free(tree);
			return false;
		}
	}
	else {
		char *end;
		size_t index = strtoul(last, &end, 10);
		assert(last[0] && !end[0]);

		if (!ece_blobtree_array_replace_item(p.parent, index, tree)) {
			ece_blobtree_free(tree);
			return false;
		}
	}

	return true;
}

/** Inserts a list of new entries into the array described by the given pointer (if valid) */
static bool eced_config_insert(eced_config_pointer_t p, const char *last, const struct blob_attr *value) {
	if (!p.schema || !p.parent || ece_blobtree_type(p.parent) != BLOBMSG_TYPE_ARRAY) {
		errno = EINVAL;
		return false;
	}

	ece_blobtree_t *tree = ece_blobtree_parse(value);
	if (!tree)
		return false;

	char *end;
	size_t index = strtoul(last, &end, 10);
	assert(last[0] && !end[0]);

	if (!eced_schema_validate_insert_items(p.parent_schema, p.parent, index, tree)) {
		ece_blobtree_free(tree);
		errno = EINVAL;
		return false;
	}

	if (!ece_blobtree_array_insert_items(p.parent, index, tree)) {
		ece_blobtree_free(tree);
		return false;
	}

	return true;
}

/** Prepend/appends lists of new entries to the array described by the given pointer (if valid) */
static bool eced_config_extend(eced_config_pointer_t p, const struct blob_attr *prepend, const struct blob_attr *append) {
	if (!p.schema || !p.object || ece_blobtree_type(p.object) != BLOBMSG_TYPE_ARRAY) {
		errno = EINVAL;
		return false;
	}
	ece_blobtree_t *prepend_tree = prepend ? ece_blobtree_parse(prepend) : NULL;
	ece_blobtree_t *append_tree = append ? ece_blobtree_parse(append) : NULL;
	if ((prepend && !prepend_tree) || (append && !append_tree)) {
		ece_blobtree_free(prepend_tree);
		ece_blobtree_free(append_tree);
		return false;
	}

	if (!eced_schema_validate_extend_items(p.schema, p.object, prepend_tree, append_tree)) {
		ece_blobtree_free(prepend_tree);
		ece_blobtree_free(append_tree);
		errno = EINVAL;
		return false;
	}

	if (prepend_tree)
		eced_assert(ece_blobtree_array_insert_items(p.object, 0, prepend_tree));

	if (append_tree)
		eced_assert(ece_blobtree_array_append_items(p.object, append_tree));

	return true;
}

static bool eced_config_reset(eced_config_pointer_t p, const char *last, const char *validify_path) {
	if (!p.schema) {
		errno = EINVAL;
		return false;
	}

	ece_blobtree_t *tree = NULL;
	bool may_delete = (ece_blobtree_type(p.parent) == BLOBMSG_TYPE_TABLE) &&
		eced_schema_validate_delete_property(p.parent_schema, last);
	if (eced_schema_has_explicit_default(p.schema) || !may_delete) {
		tree = eced_schema_get_default_at(validify_path, p.schema);
		if (!tree)
			return false;
	}

	if (ece_blobtree_type(p.parent) == BLOBMSG_TYPE_TABLE) {
		if (tree) {
			if (!ece_blobtree_table_insert_property(p.parent, last, tree)) {
				ece_blobtree_free(tree);
				return false;
			}
		}
		else {
			ece_blobtree_table_delete_property(p.parent, last);
		}
	}
	else {
		assert(tree);

		char *end;
		size_t index = strtoul(last, &end, 10);
		assert(last[0] && !end[0]);

		if (!ece_blobtree_array_replace_item(p.parent, index, tree)) {
			ece_blobtree_free(tree);
			return false;
		}
	}

	return true;
}

/** Deletes the config described by the given pointer (if valid) */
static bool eced_config_delete(eced_config_pointer_t p, const char *last) {
	if (!p.schema) {
		errno = EINVAL;
		return false;
	}

	if (ece_blobtree_type(p.parent) == BLOBMSG_TYPE_TABLE) {
		if (!eced_schema_validate_delete_property(p.parent_schema, last)) {
			errno = EINVAL;
			return false;
		}

		ece_blobtree_table_delete_property(p.parent, last);
		return true;
	}
	else {
		char *end;
		size_t index = strtoul(last, &end, 10);
		assert(last[0] && !end[0]);

		if (!eced_schema_validate_delete_item(p.parent_schema, p.parent, index)) {
			errno = EINVAL;
			return false;
		}

		eced_assert(ece_blobtree_array_delete_item(p.parent, index));
		return true;
	}
}

/** Validates and sets the config described by the given pointer */
static bool eced_config_modify_path(
	eced_config_pointer_t p, const struct blob_attr *value, const struct blob_attr *insert,
	const struct blob_attr *prepend, const struct blob_attr *append, bool reset,
	const char *validify_path, bool store_invalid, struct list_head *validify_diffs)
{
	size_t len = p.last ? strlen(p.last) : 0;
	char last[len+1];
	if (len)
		memcpy(last, p.last, len);
	last[len] = 0;

	/* If eced_config_lookup_path was successful, the last component must be valid */
	assert(eced_config_unescape_path(last));

	if (value)
		return eced_config_set(p, last, value, validify_path, store_invalid, validify_diffs);
	else if (insert)
		return eced_config_insert(p, last, insert);
	else if (prepend || append)
		return eced_config_extend(p, prepend, append);
	else if (reset)
		return eced_config_reset(p, last, validify_path);
	else
		return eced_config_delete(p, last);
}

bool eced_config_modify(
	const char *path, const struct blob_attr *value, const struct blob_attr *insert,
	const struct blob_attr *prepend, const struct blob_attr *append, bool reset,
	bool validify, bool store_invalid)
{
	if (!path[0] || (!!value + !!insert + (prepend || append) + reset) > 1) {
		/*
		  The empty path can't be replaced; only prepend and append can
		  be combined
		*/
		errno = EINVAL;
		return false;
	}

	const char *rest;

	eced_config_pointer_t p = eced_config_lookup_path(
		eced_config_root(), path, &rest, ECED_LOOKUP_F_ALLOW_MISSING|ECED_LOOKUP_F_NO_ARRAYS);

	if (p.schema)
		p = eced_config_lookup_path(p, rest, NULL, ECED_LOOKUP_F_ALLOW_MISSING);

	eced_diff_entry_t *diff_entry = NULL;

	if (p.schema && !reset) {
		size_t diff_root_len = rest - path;
		char diff_root[diff_root_len+1];
		memcpy(diff_root, path, diff_root_len);
		diff_root[diff_root_len] = 0;

		/* Allocate entry here to ensure we don't have an allocation failure after we've modified the config */
		diff_entry = eced_config_alloc_diff_entry(diff_root, NULL);
		if (!diff_entry)
			return false;
	}

	LIST_HEAD(validify_diffs);
	if (!eced_config_modify_path(
		p, value, insert, prepend, append, reset, (validify || reset) ? path : NULL, store_invalid, &validify_diffs))
	{
		free(diff_entry);
		eced_config_free_diff_list(&validify_diffs);
		return false;
	}

	if (reset) {
		assert(!diff_entry);
		assert(list_empty(&validify_diffs));

		eced_config_reset_diff(path);
	}
	else {
		if (diff_entry)
			eced_config_add_diff(diff_entry);

		eced_config_add_invalid_diff_list(&validify_diffs);
	}

	errno = 0;
	return true;
}

bool eced_config_clear_invalid(void) {
	if (list_empty(&invalid_diffs))
		return false;

	eced_config_free_diff_list(&invalid_diffs);
	return true;
}
