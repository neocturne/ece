/*
  Copyright (c) 2016, Matthias Schiffer <mschiffer@universe-factory.net>
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice,
       this list of conditions and the following disclaimer.
    2. Redistributions in binary form must reproduce the above copyright notice,
       this list of conditions and the following disclaimer in the documentation
       and/or other materials provided with the distribution.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

/**
  \file

  Functions related to the ubus connection
*/


#include "config.h"
#include "ubus.h"

#include <libubus.h>
#include <libubox/ulog.h>
#include <libubox/uloop.h>

#include <errno.h>
#include <stdlib.h>


/** The ubus context (and connection information) */
static struct ubus_auto_conn ubus = {};

/** Reusable buffer for blobmsgs */
static struct blob_buf b;


/** Converts a UNIX error code to a ubus error code */
static int eced_ubus_from_errno(int error) {
	switch (error) {
	case ENOENT:
		return UBUS_STATUS_NOT_FOUND;

	case EINVAL:
		return UBUS_STATUS_INVALID_ARGUMENT;

	default: /* ENOMEM etc. */
		return UBUS_STATUS_UNKNOWN_ERROR;
	}
}


enum {
	ATTR_SIMPLE_PATH,
	ATTR_SIMPLE_MAX
};

/** Request policy eced_ubus_handle_get  */
static const struct blobmsg_policy ece_attrs_simple[ATTR_SIMPLE_MAX] = {
	[ATTR_SIMPLE_PATH] = { .name = "path", .type = BLOBMSG_TYPE_STRING },
};

/** Returns a requested configuration value (or a whole tree) */
static int eced_ubus_handle_get(
	struct ubus_context *ctx, UNUSED struct ubus_object *obj,
	struct ubus_request_data *req, UNUSED const char *method,
	struct blob_attr *msg)
{
	struct blob_attr *attr[ATTR_SIMPLE_MAX];

	blobmsg_parse(ece_attrs_simple, ATTR_SIMPLE_MAX, attr, blob_data(msg), blob_len(msg));
	if (!attr[ATTR_SIMPLE_PATH])
		return UBUS_STATUS_INVALID_ARGUMENT;

	const char *path = blobmsg_get_string(attr[ATTR_SIMPLE_PATH]);
	eced_config_pointer_t p = eced_config_lookup_path(eced_config_root(), path, NULL, 0);
	if (!p.schema)
		return eced_ubus_from_errno(errno);

	blob_buf_init(&b, 0);
	ece_blobtree_put(&b, p.object, "value");
	ubus_send_reply(ctx, req, b.head);

	return 0;
}

/** Returns a requested configuration value (or a whole tree) */
static int eced_ubus_handle_reset(
	UNUSED struct ubus_context *ctx, UNUSED struct ubus_object *obj,
	UNUSED struct ubus_request_data *req, UNUSED const char *method,
	struct blob_attr *msg)
{
	struct blob_attr *attr[ATTR_SIMPLE_MAX];

	blobmsg_parse(ece_attrs_simple, ATTR_SIMPLE_MAX, attr, blob_data(msg), blob_len(msg));
	if (!attr[ATTR_SIMPLE_PATH])
		return UBUS_STATUS_INVALID_ARGUMENT;

	if (!eced_config_modify(
		blobmsg_get_string(attr[ATTR_SIMPLE_PATH]), NULL, NULL, NULL, NULL, true, false, false)
	)
		return eced_ubus_from_errno(errno);

	eced_config_schedule_commit();

	return 0;
}


enum {
	ATTR_SET_PATH,
	ATTR_SET_VALUE,
	ATTR_SET_INSERT,
	ATTR_SET_PREPEND,
	ATTR_SET_APPEND,
	ATTR_SET_MAX
};

/** Request policy eced_ubus_handle set  */
static const struct blobmsg_policy ece_attrs_set[ATTR_SET_MAX] = {
	[ATTR_SET_PATH]    = { .name = "path", .type = BLOBMSG_TYPE_STRING },
	[ATTR_SET_VALUE]   = { .name = "value", .type = BLOBMSG_TYPE_UNSPEC },
	[ATTR_SET_INSERT]  = { .name = "insert", .type = BLOBMSG_TYPE_UNSPEC },
	[ATTR_SET_PREPEND] = { .name = "prepend", .type = BLOBMSG_TYPE_UNSPEC },
	[ATTR_SET_APPEND]  = { .name = "append", .type = BLOBMSG_TYPE_UNSPEC },
};

/** Modifies a given configuration value */
static int eced_ubus_handle_set(
	UNUSED struct ubus_context *ctx, UNUSED struct ubus_object *obj,
	UNUSED struct ubus_request_data *req, UNUSED const char *method,
	struct blob_attr *msg)
{
	struct blob_attr *attr[ATTR_SET_MAX];

	blobmsg_parse(ece_attrs_set, ATTR_SET_MAX, attr, blob_data(msg), blob_len(msg));
	if (!attr[ATTR_SET_PATH])
		return UBUS_STATUS_INVALID_ARGUMENT;

	if (!eced_config_modify(
		blobmsg_get_string(attr[ATTR_SET_PATH]), attr[ATTR_SET_VALUE], attr[ATTR_SET_INSERT],
		attr[ATTR_SET_PREPEND], attr[ATTR_SET_APPEND], false, false, false)
	)
		return eced_ubus_from_errno(errno);

	eced_config_schedule_commit();

	return 0;
}

/** Reloads the schema directory */
static int eced_ubus_handle_reload(
	UNUSED struct ubus_context *ctx, UNUSED struct ubus_object *obj,
	UNUSED struct ubus_request_data *req, UNUSED const char *method,
	UNUSED struct blob_attr *msg)
{
	if (!eced_config_reload())
		return UBUS_STATUS_UNKNOWN_ERROR;

	return 0;
}

/** Removes all invalid diff entries and commits the new diff set */
static int eced_ubus_handle_reconcile(
	UNUSED struct ubus_context *ctx, UNUSED struct ubus_object *obj,
	UNUSED struct ubus_request_data *req, UNUSED const char *method,
	UNUSED struct blob_attr *msg)
{
	if (eced_config_clear_invalid())
		eced_config_schedule_commit();

	return 0;
}

/** Methods of the ece ubus object */
static const struct ubus_method ece_object_methods[] = {
	UBUS_METHOD("get", eced_ubus_handle_get, ece_attrs_simple),
	UBUS_METHOD("set", eced_ubus_handle_set, ece_attrs_set),
	UBUS_METHOD("reset", eced_ubus_handle_reset, ece_attrs_simple),
	UBUS_METHOD_NOARG("reload", eced_ubus_handle_reload),
	UBUS_METHOD_NOARG("reconcile", eced_ubus_handle_reconcile),
};

/** Type definition for the ece ubus object */
static struct ubus_object_type ece_object_type =
	UBUS_OBJECT_TYPE("eced", ece_object_methods);

/** The ece ubus object */
static struct ubus_object ece_object = {
	.name = "ece",
	.type = &ece_object_type,
	.methods = ece_object_methods,
	.n_methods = ARRAY_SIZE(ece_object_methods),
};


/** Is called by ubus after a connection has been opened */
static void eced_ubus_on_connect(struct ubus_context *ubus_ctx)
{
	ulog(LOG_DEBUG, "ubus connection successful\n");

	int ret = ubus_add_object(ubus_ctx, &ece_object);
	if (ret != 0) {
		ulog(LOG_ERR, "failed to publish object '%s': %s\n", ece_object.name, ubus_strerror(ret));
		exit(1);
	}
}

void eced_ubus_init(const char *path)
{
	ubus.path = path;
	ubus.cb = eced_ubus_on_connect;

	ubus_auto_connect(&ubus);
}

void eced_ubus_done(void)
{
	ubus_auto_shutdown(&ubus);
	blob_buf_free(&b);
}
