/*
  Copyright (c) 2016, Matthias Schiffer <mschiffer@universe-factory.net>
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice,
       this list of conditions and the following disclaimer.
    2. Redistributions in binary form must reproduce the above copyright notice,
       this list of conditions and the following disclaimer in the documentation
       and/or other materials provided with the distribution.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

/**
  \file

  eced's main file
*/


#include "config.h"
#include "ubus.h"

#include <libubox/ulog.h>
#include <libubox/uloop.h>

#include <stdio.h>

#include <sys/stat.h>


/** The main function of eced */
int main(int argc, char *argv[])
{
	ulog_open(ULOG_STDIO, LOG_DAEMON, "eced");

	if (argc != 3) {
		fprintf(stderr, "Usage: eced <schema dir> <diff>\n");
		return 1;
	}

	umask(077);

	uloop_init();
	if (!eced_config_init(argv[1], argv[2])) {
		ulog(LOG_ERR, "loading schemas failed\n");
		return 1;
	}

	eced_ubus_init(NULL);

	int status = uloop_run();

	eced_ubus_done();

	eced_config_commit();
	uloop_done();
	eced_config_free();

	if (status)
		raise(status);

	return 0;
}
