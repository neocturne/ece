# The Experimental Configuration Environment (working title)

See the Wiki for more information about the design.

## Building

The following dependencies need to be installed to build ECE:
* libjson-c
* libubox
* ubus

ECE uses CMake, so it can be built running the following commands
after cloning into a directory called *ece*:
```shell
mkdir ece-build
cd ece-build
cmake ../ece
make
```
